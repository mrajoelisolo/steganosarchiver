package application.cmd;

import application.frames.PersonManagementFrame;
import core.tools.ICommand;
import javax.swing.JFrame;
import mapping.profile.Profile;

/**
 *
 * @author Mitanjo
 */
public class CmdShowPersonalDataManagement implements ICommand {
    private JFrame parent;
    private Profile profile;
    
    public CmdShowPersonalDataManagement(JFrame parent, Profile profile) {
        this.parent = parent;
        this.profile = profile;
    }

    public void doing() {
        System.out.println("" + profile);
        if(parent == null || profile == null) return;

        PersonManagementFrame f = new PersonManagementFrame(parent, profile);
        parent.setVisible(false);
        f.setVisible(true);
    }
}
