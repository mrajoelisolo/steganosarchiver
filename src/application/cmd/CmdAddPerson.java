package application.cmd;

import application.frames.PersonCmdDlg;
import core.tools.ICommand;
import core.tools.IObserver;
import hibernate.cfg.HibernateUtil;
import javax.swing.JDialog;
import mapping.data.Person;
import mapping.profile.Profile;
import org.hibernate.HibernateException;
import org.hibernate.Session;
import org.hibernate.Transaction;

/**
 *
 * @author Mitanjo
 */
public class CmdAddPerson implements ICommand {
    private IObserver observer;
    private Profile profile;

    public CmdAddPerson(IObserver observer, Profile profile) {
        this.profile = profile;
        this.observer = observer;
    }

    public void doing() {
        if(profile == null) return;
        if(observer == null) return;

        Session session = HibernateUtil.getSessionFactory().getCurrentSession();
        Transaction tx = null;

        try {
            Person person = new Person();
            PersonCmdDlg f = new PersonCmdDlg((JDialog)observer, person);
            f.setVisible(true);

            if(f.isOk()) {
                tx = session.beginTransaction();

                person.setProfile(profile);
                session.save(person);

                tx.commit();

                observer.notify(person);
            }

        }catch(HibernateException e) {
            e.printStackTrace();
        }
    }
}
