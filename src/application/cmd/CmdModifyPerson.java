package application.cmd;

import application.frames.PersonCmdDlg;
import core.tools.ICommand;
import core.tools.IObserver;
import hibernate.cfg.HibernateUtil;
import javax.swing.JDialog;
import mapping.data.Person;
import org.hibernate.HibernateException;
import org.hibernate.Session;
import org.hibernate.Transaction;

/**
 *
 * @author Mitanjo
 */
public class CmdModifyPerson implements ICommand {
    private IObserver observer;
    private Person person;
    
    public CmdModifyPerson(IObserver observer, Person person) {
        this.observer = observer;
        this.person = person;
    }

    public void doing() {
        if(person == null) return;
        if(observer == null) return;

        Session session = HibernateUtil.getSessionFactory().getCurrentSession();
        Transaction tx = null;

        try {            
            PersonCmdDlg f = new PersonCmdDlg((JDialog)observer, person);
            f.setVisible(true);

            if(f.isOk()) {
                tx = session.beginTransaction();

                session.update(person);

                tx.commit();

                observer.notify(person);
            }

        }catch(HibernateException e) {
            e.printStackTrace();
        }
    }
}
