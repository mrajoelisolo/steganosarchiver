package application.cmd;

import application.frames.PersonCmdDlg;
import core.tools.ICommand;
import core.tools.IObserver;
import hibernate.cfg.HibernateUtil;
import javax.swing.JDialog;
import javax.swing.JOptionPane;
import mapping.data.Person;
import org.hibernate.HibernateException;
import org.hibernate.Session;
import org.hibernate.Transaction;

/**
 *
 * @author Mitanjo
 */
public class CmdDeletePerson implements ICommand {
    private IObserver observer;
    private Person person;
    
    public CmdDeletePerson(IObserver observer, Person person) {
        this.observer = observer;
        this.person = person;
    }

    public void doing() {
        if(person == null) return;
        if(observer == null) return;

        int res = JOptionPane.showConfirmDialog(null, "Are you sure to want delete ?", "Deleting person data", JOptionPane.YES_NO_OPTION, JOptionPane.QUESTION_MESSAGE);
        if(res != JOptionPane.YES_OPTION || person == null) return;

        Session session = HibernateUtil.getSessionFactory().getCurrentSession();
        Transaction tx = null;

        try {            
            tx = session.beginTransaction();

            session.delete(person);

            tx.commit();

            observer.notify(person);
        }catch(HibernateException e) {
            e.printStackTrace();
        }
    }
}
