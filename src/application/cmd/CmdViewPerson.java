package application.cmd;

import application.frames.PersonCmdDlg;
import core.tools.ICommand;
import javax.swing.JDialog;
import mapping.data.Person;

/**
 *
 * @author Mitanjo
 */
public class CmdViewPerson implements ICommand {
    private Person person;
    private JDialog parent;

    public CmdViewPerson(JDialog parent, Person person) {
        this.person = person;
        this.parent = parent;
    }

    public void doing() {
        if(person == null) return;
        if(parent == null) return;

        PersonCmdDlg f = new PersonCmdDlg(parent, person);
        f.setReadOnly(true);
        f.setTitle("Personal data");
        f.setVisible(true);
    }
}
