package application.cmd;

import application.frames.SettingsFrame;
import core.tools.ICommand;
import javax.swing.JFrame;

/**
 *
 * @author Mitanjo
 */
public class CmdShowSettings implements ICommand {
    private JFrame parent;
    
    public CmdShowSettings(JFrame parent) {
        this.parent = parent;
    }

    public void doing() {
        if(parent == null) return;

        SettingsFrame f = new SettingsFrame(parent);
        //parent.setVisible(false);
        f.setVisible(true);
    }
}
