package application.util;

/**
 *
 * @author Mitanjo
 */
public enum EnumConnection {
    SUPERUSER, ADMIN, USER, DENIED
}
