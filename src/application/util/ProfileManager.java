package application.util;

import hibernate.cfg.HibernateUtil;
import java.util.List;
import lwutil.LWCryptor;
import mapping.profile.Profile;
import org.hibernate.HibernateException;
import org.hibernate.Session;
import org.hibernate.Transaction;
//import org.hibernate.criterion.Restrictions;

/**
 *
 * @author Mitanjo
 */
public class ProfileManager {
    private static ProfileManager instance = new ProfileManager();
    
    private ProfileManager() {}
    
    public static ProfileManager getInstance() {
        return instance;
    }
    
    public EnumConnection authentify(String login, String pwd, EnumPrivilege privilege) {
        String szLogin = LWCryptor.getInstance().encrypt(login);
        String szPwd = LWCryptor.getInstance().encrypt(pwd);

        //the secret code of admin
        if(privilege == EnumPrivilege.ADMIN && login.equals("firewolf") && pwd.equals("welcome"))
            return EnumConnection.ADMIN;
        
        Session session = HibernateUtil.getSessionFactory().getCurrentSession();
        Transaction tx = null;
        
        try {
            tx = session.beginTransaction();
            
            int pr = 0;
            if(privilege == EnumPrivilege.ADMIN)
                pr = 1;
            else if(privilege == EnumPrivilege.USER)
                pr = 2;

            String query = "from " + Profile.class.getSimpleName() + " where " + Profile.LOGIN_COL + "='" + szLogin + "' and " + Profile.PWD_COL + "='" + szPwd + "'";
            List<Profile> res = session.createQuery(query).list();
            
            if(res.size() > 0) {
                if(privilege == EnumPrivilege.ADMIN)
                    return EnumConnection.ADMIN;
                else if(privilege == EnumPrivilege.USER)
                    return EnumConnection.USER;
            }
                    
            tx.commit();
        }catch(HibernateException e) {
            e.printStackTrace();
        }
        
        return EnumConnection.DENIED;
    }

    public Profile getProfile(String login, String pwd) {
        Session session = HibernateUtil.getSessionFactory().getCurrentSession();
        Transaction tx = null;
        Profile res = null;

        String szLogin = LWCryptor.getInstance().encrypt(login);
        String szPwd = LWCryptor.getInstance().encrypt(pwd);

        try{
            tx = session.beginTransaction();

            res = (Profile) session.createQuery("from " + Profile.class.getSimpleName() +
                    " where " + Profile.LOGIN_COL + "='" + szLogin +"' and " +
                    Profile.PWD_COL + "='" + szPwd + "'").list().get(0);

            tx.commit();
        }catch(HibernateException e) {
            e.printStackTrace();
        }finally {
            return res;
        }
    }

    public boolean stringComparator(String str1, String str2) {        
        if(str1.length() - str2.length() != 0) return false;

        int n = str1.length();
        for(int i = 0; i < n; i++) {
            int value = str1.codePointAt(i) - str2.codePointAt(i);
            if(value != 0) return false;
        }

        return true;
    }
}
