package application.shapes;

import java.awt.Color;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.Point;
import java.awt.Rectangle;
import java.awt.image.BufferedImage;
import lwcanvas.shapes.CanvasSettings;
import lwcanvas.shapes.IShape;
import lwcanvas.shapes.LWAbstractShape;
import lwcanvas.util.LWColorProvider;
import lwcanvas.util.LWImgUtil;

/**
 *
 * @author Mitanjo
 */
public class TCustomizedFrame extends LWAbstractShape implements IShape {
    private Rectangle bounds = new Rectangle();
    private int hGrid = 8;
    private int vGrid = 8;
    private BufferedImage img;

    public TCustomizedFrame(BufferedImage img, int x, int y, int wdt, int hgt) {
        bounds.setBounds(x, y, wdt, hgt);

        this.img = img;
    }

    @Override
    public void draw(Graphics g) {
        Graphics2D g2 = (Graphics2D) g;

        CanvasSettings.getInstance().saveGraphics(g2);

        LWImgUtil.getInstance().setComposite(g2, alphaValue);
        //g2.setColor(LWColorProvider.getInstance().getColor(0, 16, 0));
        //g2.fillRect(bounds.x, bounds.y, bounds.width, bounds.height);
        if(img != null)
            g.drawImage(img, bounds.x, bounds.y, bounds.width, bounds.height, null);


        g2.setColor(penColor);
        drawGrids(g2);
        //g2.drawRect(bounds.x, bounds.y, bounds.width, bounds.height);

        CanvasSettings.getInstance().restoreGraphics(g2);
    }

    private void drawGrids(Graphics g2) {
        Color cTmp = g2.getColor();
        //g2.setColor(LWColorProvider.getInstance().getColor(0, 96, 0));

        int yDiv = bounds.height/vGrid;
        int yIter = yDiv;
        int xDiv = bounds.width/hGrid;
        int xIter = xDiv;

        for(int i = 0; i < (vGrid); i++) {
            g2.drawLine(bounds.x, bounds.y + yIter, bounds.x + bounds.width, bounds.y + yIter);

            yIter += yDiv;
        }

        for(int i = 0; i < (hGrid); i++) {
            g2.drawLine(bounds.x + xIter, bounds.y, bounds.x + xIter, bounds.y + bounds.height);

            xIter += xDiv;
        }

        g2.setColor(cTmp);
    }

    @Override
    public boolean contains(Point p) {
        return false;
    }

    public void setBounds(int x, int y, int wdt, int hgt) {
        bounds.setBounds(x, y, wdt, hgt);
    }

    public Rectangle getBounds() {
        return bounds;
    }

    public void setLocation(int x, int y) {
        bounds.setLocation(x, y);
    }

    public Point getLocation() {
        return bounds.getLocation();
    }

    public void setGrid(int hGrid, int vGrid) {
        if(hGrid < 2 || vGrid < 2) return;

        this.hGrid = hGrid;
        this.vGrid = vGrid;
    }

    public void setImg(BufferedImage img) {
        this.img = img;
    }

    public BufferedImage getImg() {
        return img;
    }
}
