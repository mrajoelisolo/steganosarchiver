package application.shapes;

import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.Point;
import java.awt.Rectangle;
import lwcanvas.shapes.CanvasSettings;
import lwcanvas.shapes.IShape;
import lwcanvas.shapes.LWAbstractShape;
import lwcanvas.util.LWImgUtil;

/**
 *
 * @author Mitanjo
 */
public class TGrid extends LWAbstractShape implements IShape {
    private boolean bFalse = false;
    private Rectangle bounds = new Rectangle();

    public TGrid(int x, int y, int wdt, int hgt) {
        bounds.setBounds(x, y , wdt, hgt);
    }

    public void draw(Graphics g) {
        Graphics2D g2 = (Graphics2D) (Graphics) g;

        CanvasSettings.getInstance().saveGraphics(g2);
        LWImgUtil.getInstance().setComposite(g2, alphaValue);

        g2.setColor(penColor);
        g2.setStroke(stroke);

        int pos = bounds.width / 10;
        for(int i = 0; i < 9; i++) {
            g2.drawLine(pos, bounds.y, pos, bounds.y + bounds.height);
            pos += bounds.width / 10;
        }

        pos = bounds.height / 10;
        for(int i = 0; i < 9; i++) {
            g2.drawLine(bounds.x, pos, bounds.x + bounds.width, pos);
            pos += bounds.height / 10;
        }

        CanvasSettings.getInstance().restoreGraphics(g2);
    }

    public boolean contains(Point p) {
        return bFalse;
    }
}
