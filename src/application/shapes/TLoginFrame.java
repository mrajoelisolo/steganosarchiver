package application.shapes;

import java.awt.Color;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.Point;
import lwcanvas.shapes.CanvasSettings;
import lwcanvas.shapes.IShape;
import lwcanvas.shapes.LWAbstractShape;
import lwcanvas.shapes.LWDynamicPolygon;
import lwcanvas.shapes.LWRectangle;
import lwcanvas.util.LWAllocator;

/**
 *
 * @author Mitanjo
 */
public class TLoginFrame extends LWAbstractShape implements IShape {
    private Point pos = new Point();
    private LWDynamicPolygon frame;
    private LWRectangle login;
    private LWRectangle pwd;

    public TLoginFrame(int x, int y) {
        pos.setLocation(x, y);

        Point[] pts = LWAllocator.getInstance().allocateArrayPoint(5);

        frame = new LWDynamicPolygon(pts);
        frame.setNodeAt(0, 0, 0);
        frame.setNodeAt(1, 305, 0);
        frame.setNodeAt(2, 305, 163);
        frame.setNodeAt(3, 275, 192);
        frame.setNodeAt(4, 0, 192);
        
        login = new LWRectangle(10, 35, 250, 35);
        pwd = new LWRectangle(10, 88, 250, 35);

        frame.setBrushColor(Color.BLACK);
        login.setBrushColor(Color.BLACK);
        pwd.setBrushColor(Color.BLACK);
    }

    private void initShape() {
        frame.setLocation(pos.x, pos.y);
        login.setLocation(10 + pos.x, 35 + pos.y);
        pwd.setLocation(10 + pos.x, 88 + pos.y);
    }

    @Override
    public void draw(Graphics g) {
        initShape();

        Graphics2D g2 = (Graphics2D) g;

        CanvasSettings.getInstance().saveGraphics(g2);

        frame.setAlphaValue(alphaValue);
        frame.setPenColor(penColor);

        login.setPenColor(penColor);
        login.setAlphaValue(alphaValue);

        pwd.setPenColor(penColor);
        pwd.setAlphaValue(alphaValue);

        frame.draw(g2);
        //login.draw(g2);
        //pwd.draw(g2);

        CanvasSettings.getInstance().restoreGraphics(g2);
    }

    public boolean contains(Point p) {
        return false;
    }

    public void setLocation(int x, int y) {
        pos.setLocation(x, y);
    }

    public Point getLocation() {
        return pos;
    }
}
