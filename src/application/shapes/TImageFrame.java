package application.shapes;

import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.Image;
import java.awt.Point;
import java.awt.Rectangle;
import lwcanvas.shapes.CanvasSettings;
import lwcanvas.shapes.IShape;
import lwcanvas.shapes.LWAbstractShape;
import lwcanvas.util.LWImgUtil;

/**
 *
 * @author Mitanjo
 */
public class TImageFrame extends LWAbstractShape implements IShape {
    private boolean bContains = false;
    private Rectangle bounds = new Rectangle();
    private Image image;

    public TImageFrame(Image image, int x, int y, int wdt, int hgt) {
        this.image = image;
        bounds.setBounds(x, y, wdt, hgt);
    }

    public void draw(Graphics g) {
        Graphics2D g2 = (Graphics2D) g;

        CanvasSettings.getInstance().saveGraphics(g2);

        LWImgUtil.getInstance().setComposite(g2, alphaValue);
        if(image != null)
            g2.drawImage(image, bounds.x, bounds.y, bounds.width, bounds.height, null);

        CanvasSettings.getInstance().restoreGraphics(g2);
    }

    public boolean contains(Point p) {
        return bContains;
    }

    public void setLocation(int x, int y) {
        bounds.setLocation(x, y);
    }

    Point bufPoint = new Point();
    public Point getLocation() {
        bufPoint.setLocation(bounds.x, bounds.y);
        return bufPoint;
    }

    public void setBounds(int x, int y, int wdt, int hgt) {
        bounds.setBounds(x, y, wdt, hgt);
    }

    public Rectangle getBounds() {
        return bounds;
    }
}
