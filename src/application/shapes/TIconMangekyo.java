package application.shapes;

import java.awt.Color;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.Point;
import lwcanvas.shapes.CanvasSettings;
import lwcanvas.shapes.IShape;
import lwcanvas.shapes.LWAbstractFilledShape;
import lwcanvas.shapes.LWAbstractShape;
import lwcanvas.shapes.LWCircle;
import lwcanvas.shapes.LWPolygon;
import lwcanvas.util.LWAllocator;
import lwcanvas.util.LWGeom;
import lwcanvas.util.LWPointUtilities;

/**
 *
 * @author Mitanjo
 */
public class TIconMangekyo extends LWAbstractShape implements IShape {
    private Point pos = new Point();
    private Branch branch1;
    private Branch branch2;
    private Branch branch3;
    private LWCircle circumference;
    private LWCircle center;
    private boolean visible = true;
    private double theta = 0;

    public TIconMangekyo(int x, int y) {
        pos.setLocation(x, y);

        branch1 = new Branch(0, 0);
        branch2 = new Branch(0, 0);
        branch3 = new Branch(0, 0);
        circumference = new LWCircle(0, 0, 106);
        center = new LWCircle(0, 0, 16);

        branch1.setBrushColor(Color.BLACK);

        branch2.setBrushColor(Color.BLACK);
        
        branch3.setBrushColor(Color.BLACK);

        circumference.setLocation(pos.x, pos.y);
        //circumference.setBrushColor(Color.BLACK);
        circumference.setPenColor(penColor);

        center.setLocation(pos.x, pos.y);
        center.setPenColor(penColor);
        center.setBrushColor(Color.BLACK);
    }

    private void initShape() {
        //pBuf.setLocation(pos);
        branch1.setLocation(pos.x, pos.y);
        branch1.setTheta(theta);

        branch2.setLocation(pos.x, pos.y);
        branch2.setTheta(theta + 2*Math.PI/3);

        branch3.setLocation(pos.x, pos.y);
        branch3.setTheta(theta + 4*Math.PI/3);
    }

    public void draw(Graphics g) {
        if(!visible) return;

        initShape();

        Graphics2D g2 = (Graphics2D) g;

        CanvasSettings.getInstance().saveGraphics(g2);

        circumference.setAlphaValue(alphaValue);
        circumference.setPenColor(penColor);
        circumference.draw(g2);

        branch1.setAlphaValue(alphaValue);
        branch1.setPenColor(penColor);
        branch1.draw(g2);

        branch2.setAlphaValue(alphaValue);
        branch2.setPenColor(penColor);
        branch2.draw(g2);

        branch3.setAlphaValue(alphaValue);
        branch3.setPenColor(penColor);
        branch3.draw(g2);

        center.setAlphaValue(alphaValue);
        center.setPenColor(penColor);
        center.draw(g2);

        CanvasSettings.getInstance().restoreGraphics(g2);
    }

    public boolean contains(Point p) {
        return false;
    }

    public void setTheta(double theta) {
        this.theta = theta;
    }

    public void turn(double incr) {
        theta -= incr;
    }

    public boolean isVisible() {
        return visible;
    }

    public void setVisible(boolean visible) {
        this.visible = visible;
    }

    public void setBrushColor(Color brushColor) {
        branch1.setBrushColor(brushColor);
        branch2.setBrushColor(brushColor);
        branch3.setBrushColor(brushColor);
        circumference.setBrushColor(brushColor);
    }

    public void setLocation(int x, int y) {
        this.pos.setLocation(x, y);
        circumference.setLocation(x, y);
        center.setLocation(x, y);
    }

    private class Branch extends LWAbstractFilledShape implements IShape {
        private Point pos = new Point();
        private LWPolygon shape;
        private double theta = 0;
        private Point pBuf = new Point();
        private Point[] tomoeData;
        private String path = "img/icons/";

        public Branch(int x, int y) {
            pos.setLocation(x ,y);
            tomoeData = LWPointUtilities.getInstance().fileToPoints(path + "mangekyo.vrx", ",");
            shape = new LWPolygon(LWAllocator.getInstance().allocateArrayPoint(tomoeData.length));
        }

        private void initShape() {
            for(int i = 0; i < shape.getNodeCount(); i++) {
                pBuf.setLocation(pos);
                LWGeom.getInstance().translate(pBuf, tomoeData[i].x, tomoeData[i].y, theta + Math.PI/2);
                shape.setNodeAt(i, pBuf.x, pBuf.y);
            }
        }

        public void draw(Graphics g) {
            initShape();

            Graphics2D g2 = (Graphics2D) g;

            CanvasSettings.getInstance().saveGraphics(g2);

            shape.setBrushColor(this.brushColor);
            shape.setPenColor(penColor);
            shape.setFilled(filled);
            shape.setAlphaValue(alphaValue);
            shape.draw(g2);

            CanvasSettings.getInstance().restoreGraphics(g2);
        }

        public boolean contains(Point p) {
            return false;
        }

        private void setTheta(double theta) {
            this.theta = theta;
        }

        public void setLocation(int x, int y) {
            pos.setLocation(x, y);
        }
    }
}
