package application.shapes;

import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.Point;
import lwcanvas.shapes.CanvasSettings;
import lwcanvas.shapes.IShape;
import lwcanvas.shapes.LWAbstractShape;
import lwcanvas.shapes.LWDynamicPolygon;

/**
 *
 * @author Mitanjo
 */
public class TAboveGate extends LWAbstractShape implements IShape {
    private LWDynamicPolygon frame;
    private int yEndLim = -285;

    public TAboveGate() {
        Point[] pts = new Point[6];
        pts[0] = new Point(0, 0);
        pts[1] = new Point(0, 315);
        pts[2] = new Point(368, 315);
        pts[3] = new Point(432, 285);
        pts[4] = new Point(800, 285);
        pts[5] = new Point(800, 0);
        frame = new LWDynamicPolygon(pts);
    }

    public void draw(Graphics g) {
        Graphics2D g2 = (Graphics2D) g;

        CanvasSettings.getInstance().saveGraphics(g2);

        frame.setAlphaValue(alphaValue);
        frame.draw(g);

        CanvasSettings.getInstance().restoreGraphics(g2);
    }

    public void setLocation(int x, int y) {
        frame.setLocation(x, y);
    }

    public Point getLocation() {
        return frame.getLocation();
    }

    public boolean contains(Point point) {
        return false;
    }

    public int getYEndLim() {
        return yEndLim;
    }

    public void setYEndLim(int yEndLim) {
        this.yEndLim = yEndLim;
    }

    public synchronized void up(int incr) {
        Point p = getLocation();
        if(p.y >= getYEndLim()) {
            setLocation(p.x, p.y - incr);
        }
    }
}
