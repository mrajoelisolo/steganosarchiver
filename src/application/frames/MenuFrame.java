package application.frames;

import application.shapes.*;
import application.cmd.CmdShowPersonalDataManagement;
import application.cmd.CmdShowSettings;
import core.tools.ThemeManager;
import java.awt.Graphics;
import java.awt.Image;
import javax.swing.*;
import lwcanvas.LWCanvas;
import lwcanvas.components.LWButton;
import lwutil.LWResourceLoader;
import mapping.profile.Profile;
import application.util.*;
import core.encryption.KeySetting;
import core.encryption.KeySettingsLoader;
import core.encryption.SteganosCryptor;
import core.tools.Invoker;

/**
 *
 * @author Mitanjo
 */
public class MenuFrame extends JFrame implements Runnable {
    private Thread myThread;
    private LWCanvas canvas;
    private Image imgBcg;
    private TImageFrame imgFrame1;
    private TImageFrame imgFrame2;
    private TAboveGate aboveGate;
    private TBelowGate belowGate;
    private int gateSpd = 3;
    private LWButton btnMenu1;
    private LWButton btnMenu2;
    private LWButton btnMenu3;
    private int vScroll1 = 2;
    private int vScroll2 = 2;

    private TLoginFrame loginFrame;
    private JTextField txtLogin;
    private JPasswordField txtPwd;
    private LWButton btnOk;
    private LWButton btnCancel;

    private boolean logged = false;
    private boolean isReady = false;

    private Profile currentProfile;

    public MenuFrame() {
        initComponents();
        initThread();

        KeySetting ks = KeySettingsLoader.getInstance().loadSettings();
        SteganosCryptor.getInstance().initialize(ks.getStrKey(), ks.getImgKey(), ks.getThresold(), ks.getThresold());
    }

    private void initThread() {
        myThread = new Thread(this);
        myThread.start();
    }

    private void initComponents() {
        setTitle("Menu");
        setSize(807, 634);
        setLocationRelativeTo(null);
        setResizable(false);
        setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        getContentPane().setLayout(null);

        imgBcg = LWResourceLoader.getInstance().loadImage("img/menu/warpBcg.jpg");
        canvas = new LWCanvas() {
            @Override
            public void paintBackground(Graphics g) {
               if(imgBcg != null)
                   g.drawImage(imgBcg, 0, 0, getBounds().width, getBounds().height, null);
            }
        };
        canvas.setBounds(0, 0, 802, 602);
        canvas.setLayout(null);
        canvas.setRenderQuality(LWCanvas.RENDER_HIGH_QUALITY);
        canvas.setAntialiasing(LWCanvas.ANTIALIASING_ON);
        getContentPane().add(canvas);

        imgFrame1 = new TImageFrame(LWResourceLoader.getInstance().loadImage("img/menu/txt1.png"), 0, -600, 800, 1200);
        imgFrame1.setAlphaValue(0.5f);
        canvas.addShape(imgFrame1);

        imgFrame2 = new TImageFrame(LWResourceLoader.getInstance().loadImage("img/menu/txt2.png"), 0, 0, 800, 1200);
        imgFrame2.setAlphaValue(0.5f);
        canvas.addShape(imgFrame2);

        btnMenu1 = new LWButton("Personnal data management") {
            @Override
            public void actionPerformed() {
                btnMenu1ActionPerformed();
            }
        };
        btnMenu1.setBounds(20, 50, 350, 40);
        btnMenu1.setAlphaValue(0.6f);
        canvas.addShape(btnMenu1);

        btnMenu2 = new LWButton("Settings") {
            @Override
            public void actionPerformed() {
                btnMenu2ActionPerformed();
            }
        };
        btnMenu2.setBounds(20, 120, 350, 40);
        btnMenu2.setAlphaValue(0.6f);
        canvas.addShape(btnMenu2);

        btnMenu3 = new LWButton("About") {
            @Override
            public void actionPerformed() {
                btnMenu3ActionPerformed();
            }
        };
        btnMenu3.setBounds(20, 190, 350, 40);
        btnMenu3.setAlphaValue(0.6f);
        canvas.addShape(btnMenu3);

        aboveGate = new TAboveGate();
        aboveGate.setLocation(0, 0);
        aboveGate.setAlphaValue(0.6f);
        canvas.addShape(aboveGate);

        belowGate = new TBelowGate();
        belowGate.setLocation(0, 285);
        belowGate.setAlphaValue(0.6f);
        canvas.addShape(belowGate);

        loginFrame = new TLoginFrame(251, 221);
        loginFrame.setAlphaValue(0.7f);
        canvas.addShape(loginFrame);

        txtLogin = new JTextField();
        txtLogin.setBounds(271, 256, 250, 35);
        ThemeManager.getInstance().setTextFieldToCurrentTheme(txtLogin);
        canvas.add(txtLogin);

        txtPwd = new JPasswordField();
        txtPwd.setBounds(271, 308, 250, 35);
        ThemeManager.getInstance().setTextFieldToCurrentTheme(txtPwd);
        canvas.add(txtPwd);

        btnOk = new LWButton("Login") {
            @Override
            public void actionPerformed() {
                btnOkActionPerformed();
            }
        };
        btnOk.setBounds(271, 363, 100, 30);
        canvas.addShape(btnOk);

        btnCancel = new LWButton("Cancel") {
            @Override
            public void actionPerformed() {
                btnCancelActionPerformed();
            };
        };
        btnCancel.setBounds(416, 363, 100, 30);
        canvas.addShape(btnCancel);


        setMenuEnabled(false);
    }

    private void core() {
        if(logged) {
            aboveGate.up(gateSpd);
            belowGate.down(gateSpd);
        }

        if(imgFrame1.getLocation().y < 0) {
            imgFrame1.setLocation(imgFrame1.getLocation().x, imgFrame1.getLocation().y + vScroll1);
        }else
            imgFrame1.setLocation(0, -600);

        if(imgFrame2.getLocation().y > -500) {
            imgFrame2.setLocation(imgFrame2.getLocation().x, imgFrame2.getLocation().y - vScroll2);
        }else
            imgFrame2.setLocation(0, 0);

        if(aboveGate.getLocation().y <= aboveGate.getYEndLim()
                && belowGate.getLocation().y >= belowGate.getYEndLim()
                    && !isReady) {
            isReady = true;
            setMenuEnabled(true);
        }

        canvas.repaint();
    }

    private void setMenuEnabled(boolean enabled) {
        btnMenu1.setEnabled(enabled);
        btnMenu2.setEnabled(enabled);
        btnMenu3.setEnabled(enabled);
    }

    private void setLoginFrameVisible(boolean visible) {
        txtLogin.setVisible(visible);
        txtPwd.setVisible(visible);
        
        if(visible) {
            btnOk.setLocation(271, 363);
            btnCancel.setLocation(416, 363);
            loginFrame.setLocation(251, 221);
        }else{
            btnOk.setLocation(-300, -300);
            btnCancel.setLocation(-300, -300);
            loginFrame.setLocation(-300, -300);
        }
    }

    private void btnOkActionPerformed() {
        String login = txtLogin.getText();
        String pwd = String.valueOf(txtPwd.getPassword());

        if(login.trim().equals("") && pwd.trim().equals(""))
            return;

        EnumConnection res = ProfileManager.getInstance().authentify(login, pwd, EnumPrivilege.ADMIN);
        if(res == EnumConnection.ADMIN) {
            setLoginFrameVisible(false);
            logged = true;
            currentProfile = ProfileManager.getInstance().getProfile(login, pwd);
        }else
            JOptionPane.showMessageDialog(null, "Denied", "", JOptionPane.INFORMATION_MESSAGE);
    }

    private void btnCancelActionPerformed() {

    }

    private void btnMenu1ActionPerformed() {
        CmdShowPersonalDataManagement cmd = new CmdShowPersonalDataManagement(this, currentProfile);
        Invoker.getInstance().invoke(cmd);
    }

    private void btnMenu2ActionPerformed() {
        CmdShowSettings cmd = new CmdShowSettings(this);
        Invoker.getInstance().invoke(cmd);
    }

    private void btnMenu3ActionPerformed() {
        
    }

    public void run() {
        while(true) {
            try {
                if(myThread.equals(Thread.currentThread())) {
                    core();
                    Thread.sleep(30);
                }
            }catch(Exception e) {
                e.printStackTrace();
            }
        }
    }
}
