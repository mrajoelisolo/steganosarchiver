/*
 * FolderCmdFrame.java
 *
 * Created on 16 avr. 2010, 16:09:56
 */

package application.frames;

import java.util.Date;
import javax.swing.JDialog;
import lwutil.LWCalendarPicker;
import lwutil.LWCalendarUtil;
import mapping.data.Person;

/**
 *
 * @author Mitanjo
 */
public class PersonCmdDlg extends javax.swing.JDialog {
    private boolean ok = false;
    private Person person;
    private JDialog parent;

    public PersonCmdDlg(JDialog parent, Person person) {
        super(parent, true);
        initComponents();
        initFrame();

        this.parent = parent;
        this.person = person;

        if(person.getId() == null) {//Si l'objet est nouveau
            setTitle("Create new person");
        }else {
            txtFirstName.setText(person.getSzFirstName());
            txtLastName.setText(person.getSzLastName());
            txtBirthDate.setText(person.getSzBirthDate());
            txtAddress.setText(person.getSzAddress());
            txtContact.setText(person.getSzContact());
            txtDescription.setText(person.getSzDescription());
            setTitle("Modify a person");
        }
    }

    private void initFrame() {
        this.setLocationRelativeTo(parent);
    }

    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        panelContainer = new javax.swing.JPanel();
        btnOk = new javax.swing.JButton();
        btnCancel = new javax.swing.JButton();
        panelData = new javax.swing.JPanel();
        txtFirstName = new javax.swing.JTextField();
        lblFirstName = new javax.swing.JLabel();
        jScrollPane1 = new javax.swing.JScrollPane();
        txtDescription = new javax.swing.JTextArea();
        txtLastName = new javax.swing.JTextField();
        lblLastName = new javax.swing.JLabel();
        txtBirthDate = new javax.swing.JTextField();
        lblBirthDate = new javax.swing.JLabel();
        btnBrowseBirthDate = new javax.swing.JButton();
        txtAddress = new javax.swing.JTextField();
        lblAddress = new javax.swing.JLabel();
        txtContact = new javax.swing.JTextField();
        lblContact = new javax.swing.JLabel();

        setDefaultCloseOperation(javax.swing.WindowConstants.DISPOSE_ON_CLOSE);
        setMinimumSize(new java.awt.Dimension(800, 600));
        setResizable(false);

        panelContainer.setBackground(new java.awt.Color(0, 0, 0));
        panelContainer.setName("panelContainer"); // NOI18N
        panelContainer.setLayout(new org.netbeans.lib.awtextra.AbsoluteLayout());

        btnOk.setBackground(new java.awt.Color(0, 255, 0));
        btnOk.setFont(new java.awt.Font("Tahoma", 1, 11));
        btnOk.setText("OK");
        btnOk.setName("btnOk"); // NOI18N
        btnOk.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnOkActionPerformed(evt);
            }
        });
        panelContainer.add(btnOk, new org.netbeans.lib.awtextra.AbsoluteConstraints(610, 560, 80, 30));

        btnCancel.setBackground(new java.awt.Color(0, 255, 0));
        btnCancel.setFont(new java.awt.Font("Tahoma", 1, 11));
        btnCancel.setText("Cancel");
        btnCancel.setName("btnCancel"); // NOI18N
        btnCancel.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnCancelActionPerformed(evt);
            }
        });
        panelContainer.add(btnCancel, new org.netbeans.lib.awtextra.AbsoluteConstraints(710, 560, 80, 30));

        panelData.setBackground(new java.awt.Color(0, 0, 0));
        panelData.setBorder(javax.swing.BorderFactory.createEtchedBorder());
        panelData.setName("panelData"); // NOI18N
        panelData.setLayout(new org.netbeans.lib.awtextra.AbsoluteLayout());

        txtFirstName.setBackground(new java.awt.Color(0, 0, 0));
        txtFirstName.setForeground(new java.awt.Color(0, 255, 0));
        txtFirstName.setName("txtFirstName"); // NOI18N
        panelData.add(txtFirstName, new org.netbeans.lib.awtextra.AbsoluteConstraints(110, 10, 660, 30));

        lblFirstName.setFont(new java.awt.Font("Tahoma", 1, 11));
        lblFirstName.setForeground(java.awt.Color.green);
        lblFirstName.setText("First name");
        lblFirstName.setName("lblFirstName"); // NOI18N
        panelData.add(lblFirstName, new org.netbeans.lib.awtextra.AbsoluteConstraints(10, 10, 70, 30));

        jScrollPane1.setName("jScrollPane1"); // NOI18N

        txtDescription.setBackground(new java.awt.Color(0, 0, 0));
        txtDescription.setColumns(20);
        txtDescription.setForeground(new java.awt.Color(0, 255, 0));
        txtDescription.setRows(5);
        txtDescription.setName("txtDescription"); // NOI18N
        jScrollPane1.setViewportView(txtDescription);

        panelData.add(jScrollPane1, new org.netbeans.lib.awtextra.AbsoluteConstraints(10, 260, 760, 270));

        txtLastName.setBackground(new java.awt.Color(0, 0, 0));
        txtLastName.setForeground(new java.awt.Color(0, 255, 0));
        txtLastName.setName("txtLastName"); // NOI18N
        panelData.add(txtLastName, new org.netbeans.lib.awtextra.AbsoluteConstraints(110, 50, 660, 30));

        lblLastName.setFont(new java.awt.Font("Tahoma", 1, 11));
        lblLastName.setForeground(new java.awt.Color(0, 255, 0));
        lblLastName.setText("Last name");
        lblLastName.setName("lblLastName"); // NOI18N
        panelData.add(lblLastName, new org.netbeans.lib.awtextra.AbsoluteConstraints(10, 50, 70, 30));

        txtBirthDate.setBackground(new java.awt.Color(0, 0, 0));
        txtBirthDate.setEditable(false);
        txtBirthDate.setForeground(new java.awt.Color(0, 255, 0));
        txtBirthDate.setName("txtBirthDate"); // NOI18N
        panelData.add(txtBirthDate, new org.netbeans.lib.awtextra.AbsoluteConstraints(110, 100, 230, 30));

        lblBirthDate.setFont(new java.awt.Font("Tahoma", 1, 11));
        lblBirthDate.setForeground(new java.awt.Color(0, 255, 0));
        lblBirthDate.setText("Birth date");
        lblBirthDate.setName("lblBirthDate"); // NOI18N
        panelData.add(lblBirthDate, new org.netbeans.lib.awtextra.AbsoluteConstraints(10, 100, 70, 30));

        btnBrowseBirthDate.setBackground(new java.awt.Color(0, 255, 0));
        btnBrowseBirthDate.setFont(new java.awt.Font("Tahoma", 1, 11)); // NOI18N
        btnBrowseBirthDate.setText("...");
        btnBrowseBirthDate.setName("btnBrowseBirthDate"); // NOI18N
        btnBrowseBirthDate.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnBrowseBirthDateActionPerformed(evt);
            }
        });
        panelData.add(btnBrowseBirthDate, new org.netbeans.lib.awtextra.AbsoluteConstraints(350, 100, 50, 30));

        txtAddress.setBackground(new java.awt.Color(0, 0, 0));
        txtAddress.setForeground(new java.awt.Color(0, 255, 0));
        txtAddress.setName("txtAddress"); // NOI18N
        panelData.add(txtAddress, new org.netbeans.lib.awtextra.AbsoluteConstraints(110, 150, 290, 30));

        lblAddress.setFont(new java.awt.Font("Tahoma", 1, 11));
        lblAddress.setForeground(new java.awt.Color(0, 255, 0));
        lblAddress.setText("Address");
        lblAddress.setName("lblAddress"); // NOI18N
        panelData.add(lblAddress, new org.netbeans.lib.awtextra.AbsoluteConstraints(10, 150, 70, 30));

        txtContact.setBackground(new java.awt.Color(0, 0, 0));
        txtContact.setForeground(new java.awt.Color(0, 255, 0));
        txtContact.setName("txtContact"); // NOI18N
        panelData.add(txtContact, new org.netbeans.lib.awtextra.AbsoluteConstraints(110, 200, 290, 30));

        lblContact.setFont(new java.awt.Font("Tahoma", 1, 11));
        lblContact.setForeground(new java.awt.Color(0, 255, 0));
        lblContact.setText("Contact");
        lblContact.setName("lblContact"); // NOI18N
        panelData.add(lblContact, new org.netbeans.lib.awtextra.AbsoluteConstraints(10, 200, 70, 30));

        panelContainer.add(panelData, new org.netbeans.lib.awtextra.AbsoluteConstraints(10, 10, 780, 540));

        getContentPane().add(panelContainer, java.awt.BorderLayout.CENTER);

        pack();
    }// </editor-fold>//GEN-END:initComponents

    private void btnOkActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnOkActionPerformed
        if(txtFirstName.getText().trim().equals("")) return;

        person.setSzFirstName(txtFirstName.getText());
        person.setSzLastName(txtLastName.getText());
        person.setSzBirthDate(txtBirthDate.getText());
        person.setSzAddress(txtAddress.getText());
        person.setSzContact(txtContact.getText());
        person.setSzDescription(txtDescription.getText());

        ok = true;
        dispose();
    }//GEN-LAST:event_btnOkActionPerformed

    private void btnCancelActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnCancelActionPerformed
        dispose();
    }//GEN-LAST:event_btnCancelActionPerformed

    private void btnBrowseBirthDateActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnBrowseBirthDateActionPerformed
        LWCalendarPicker datePicker = new LWCalendarPicker(this);
        datePicker.setVisible(true);

        if(datePicker.isOk()) {
            Date date = datePicker.getDate();
            txtBirthDate.setText(LWCalendarUtil.getInstance().dateToString(date));
        }
    }//GEN-LAST:event_btnBrowseBirthDateActionPerformed

    public boolean isOk() {
        return ok;
    }

    public void setReadOnly(boolean b) {
        txtFirstName.setEditable(!b);
        txtLastName.setEditable(!b);
        txtAddress.setEditable(!b);
        txtContact.setEditable(!b);
        txtDescription.setEditable(!b);
    }

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton btnBrowseBirthDate;
    private javax.swing.JButton btnCancel;
    private javax.swing.JButton btnOk;
    private javax.swing.JScrollPane jScrollPane1;
    private javax.swing.JLabel lblAddress;
    private javax.swing.JLabel lblBirthDate;
    private javax.swing.JLabel lblContact;
    private javax.swing.JLabel lblFirstName;
    private javax.swing.JLabel lblLastName;
    private javax.swing.JPanel panelContainer;
    private javax.swing.JPanel panelData;
    private javax.swing.JTextField txtAddress;
    private javax.swing.JTextField txtBirthDate;
    private javax.swing.JTextField txtContact;
    private javax.swing.JTextArea txtDescription;
    private javax.swing.JTextField txtFirstName;
    private javax.swing.JTextField txtLastName;
    // End of variables declaration//GEN-END:variables

}
