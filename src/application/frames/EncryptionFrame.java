package application.frames;

import core.encryption.KeySetting;
import core.encryption.KeySettingsLoader;
import core.markrecon.ReconModuleOne;
import core.encryption.SteganosCryptor;
import java.awt.Image;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.image.BufferedImage;
import javax.swing.*;
import lwcanvas.util.LWImgUtil;
import lwutil.LWResourceLoader;

/**
 *
 * @author Mitanjo
 */
public class EncryptionFrame extends JFrame {
    private JScrollPane scrollPane1;
    private JScrollPane scrollPane2;
    private JTextPane txtPane1;
    private JTextPane txtPane2;
    private JButton btnEncrypt;
    private JButton btnDecrypt;

    private Image keyImage;
    private ReconModuleOne reconModule;

    public EncryptionFrame() {
        initComponents();
        initEncryptor();
    }

    private void initComponents() {
        setTitle("Quality test");
        setSize(640, 507);
        setLocationRelativeTo(null);
        setResizable(false);
        setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        getContentPane().setLayout(null);

        scrollPane1 = new JScrollPane();
        scrollPane1.setBounds(8, 70, 620, 195);
        getContentPane().add(scrollPane1);

        scrollPane2 = new JScrollPane();
        scrollPane2.setBounds(8, 275, 620, 195);
        getContentPane().add(scrollPane2);

        txtPane1 = new JTextPane();
        txtPane1.setBorder(BorderFactory.createEtchedBorder());
        txtPane1.setBounds(8, 70, 620, 195);
        scrollPane1.setViewportView(txtPane1);

        txtPane2 = new JTextPane();
        txtPane2.setBorder(BorderFactory.createEtchedBorder());
        txtPane2.setBounds(8, 275, 620, 195);
        scrollPane2.setViewportView(txtPane2);

        btnEncrypt = new JButton("Encrypt");
        btnEncrypt.setBounds(10, 10, 100, 30);
        btnEncrypt.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                btnEncryptActionPerformed(e);
            }
        });
        getContentPane().add(btnEncrypt);

        btnDecrypt = new JButton("Decrypt");
        btnDecrypt.setBounds(120, 10, 100, 30);
        btnDecrypt.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                btnDecryptActionPerformed(e);
            }
        });
        getContentPane().add(btnDecrypt);
    }

    private void initEncryptor() {
//        reconModule = new ReconModuleOne(15, 100);
//        keyImage = LWResourceLoader.getInstance().loadImage("img/naruhina.jpg");
//        Encryptor.getInstance().initialize("c3df32ea", keyImage, reconModule);

        BufferedImage imgKey = LWImgUtil.getInstance().toBufferedImage(LWResourceLoader.getInstance().loadImage("img/key/key.jpg"));
        SteganosCryptor.getInstance().initialize("t4ed6p", imgKey, 100, 5);

//        KeySetting ks = new KeySetting();
//        ks.setDivision(5);
//        ks.setImgKey(imgKey);
//        ks.setStrKey("t4ed6p");
//        ks.setThresold(100);
//        KeySettingsLoader.getInstance().saveSettings(ks);
    }

    private void btnEncryptActionPerformed(ActionEvent evt) {
        //String res = Encryptor.getInstance().encrypt(txtPane1.getText());
        String res = SteganosCryptor.getInstance().encrypt(txtPane1.getText());
        txtPane2.setText(res);
    }

    private void btnDecryptActionPerformed(ActionEvent evt) {
        //String res = Encryptor.getInstance().decrypt(txtPane1.getText());
        String res = SteganosCryptor.getInstance().decrypt(txtPane1.getText());
        txtPane2.setText(res);
    }
}
