package application.frames;

import application.cmd.*;
import core.tools.IObserver;
import core.tools.Invoker;
import java.awt.Graphics;
import java.awt.Image;
import java.awt.event.*;
import javax.swing.*;
import lwcanvas.LWCanvas;
import lwcanvas.components.LWButton;
import lwdatabase.*;
import lwutil.LWResourceLoader;
import mapping.data.Person;
import mapping.profile.Profile;

/**
 *
 * @author Mitanjo
 */
public class PersonManagementFrame extends JDialog implements Runnable, IObserver {
    private Thread myThread;
    private JFrame parent;
    private Profile profile;
    private LWCanvas canvas;
    private Image imgBcg;

    private JScrollPane scrollPane1;
    private JTable table1;

    private EntityModel em;
    private LWRecordSet rSet = new LWRecordSet();
    private Person selectedPerson;

    private LWButton btnAdd;
    private LWButton btnDelete;
    private LWButton btnModify;
    private LWButton btnView;

    public PersonManagementFrame(JFrame parent, Profile profile) {
        super(parent, true);

        this.parent = parent;
        this.profile = profile;

        initFrame();
        initThread();

        em = new EntityModel(new Person());
        em.addColumnString(new EColumnModel("Name", Person.SZFIRSTNAME_COL));
        em.addColumnString(new EColumnModel("Description", Person.SZLASTNAME_COL));
        DataLoader.getInstance().loadData(this.table1, rSet, em);
    }

    private void initFrame() {
        setTitle("Personal Data Management");
        setSize(807, 634);
        setLocationRelativeTo(null);
        setResizable(false);
        getContentPane().setLayout(null);

        WindowAdapter wa = new WindowAdapter() {
            @Override
            public void windowClosing(WindowEvent evt) {
                formWindowClosing(evt);
            }
        };
        this.addWindowListener(wa);

        imgBcg = LWResourceLoader.getInstance().loadImage("img/menu/warpBcg.jpg");
        canvas = new LWCanvas() {
            @Override
            public void paintBackground(Graphics g) {
               if(imgBcg != null)
                   g.drawImage(imgBcg, 0, 0, getBounds().width, getBounds().height, null);
            }
        };
        canvas.setBounds(0, 0, 802, 602);
        canvas.setRenderQuality(LWCanvas.RENDER_HIGH_QUALITY);
        canvas.setAntialiasing(LWCanvas.ANTIALIASING_ON);
        getContentPane().add(canvas);

        scrollPane1 = new JScrollPane();
        scrollPane1.setBounds(147, 20, 630, 580);
        canvas.add(scrollPane1);

        table1 = new JTable();
        scrollPane1.setViewportView(table1);
        table1.addMouseListener(new MouseAdapter() {
            @Override
            public void mouseClicked(MouseEvent evt) {
                table1MouseClicked(evt);
            }
        });

        btnAdd = new LWButton("Add") {
            @Override
            public void actionPerformed() {
                btnAddActionPerformed();
            }
        };
        btnAdd.setBounds(10, 10, 100, 30);
        canvas.addShape(btnAdd);

        btnModify = new LWButton("Modify") {
            @Override
            public void actionPerformed() {
                btnModifyPersonActionPerformed();
            }
        };
        btnModify.setBounds(10, 50, 100, 30);
        canvas.addShape(btnModify);

        btnDelete = new LWButton("Delete") {
            @Override
            public void actionPerformed() {
                btnDeletePersonActionPerformed();
            }
        };
        btnDelete.setBounds(10, 90, 100, 30);
        canvas.addShape(btnDelete);

        btnView = new LWButton("View") {
            @Override
            public void actionPerformed() {
                btnViewPersonActionPerformed();
            }
        };
        btnView.setBounds(10, 130, 100, 30);
        canvas.addShape(btnView);
    }

    private void table1MouseClicked(MouseEvent evt) {
        LWRecordSetManager.getInstance().setCurrentIndex(this.table1);
        int i = LWRecordSetManager.getInstance().getCurrentIndex();
        if(rSet.size() > 0)
            selectedPerson = (Person) rSet.getValueAt(i);
        else
            selectedPerson = null;
    }

    private void initThread() {
        myThread = new Thread(this);
        myThread.start();
    }

    private void btnAddActionPerformed() {
        CmdAddPerson cmd = new CmdAddPerson(this, profile);
        Invoker.getInstance().invoke(cmd);
    }

    private void btnModifyPersonActionPerformed() {
        CmdModifyPerson cmd = new CmdModifyPerson(this, selectedPerson);
        Invoker.getInstance().invoke(cmd);
    }

    private void btnDeletePersonActionPerformed() {
        CmdDeletePerson cmd = new CmdDeletePerson(this, selectedPerson);
        Invoker.getInstance().invoke(cmd);
    }

    private void btnViewPersonActionPerformed() {
        CmdViewPerson cmd = new CmdViewPerson(this, selectedPerson);
        Invoker.getInstance().invoke(cmd);
    }

    private void core() {
        canvas.repaint();
    }

    public void run() {
        while(true) {
            try{                
                if(myThread.equals(Thread.currentThread())) {
                    Thread.sleep(30);
                    core();
                }
            }catch(Exception e) {
                e.printStackTrace();
            }
        }
    }

    private void formWindowClosing(WindowEvent evt) {
        parent.setVisible(true);
    }

    public void notify(Object o) {
        DataLoader.getInstance().loadData(this.table1, rSet, em);
    }
}
