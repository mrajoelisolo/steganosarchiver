/*
 * SettingsFrame.java
 *
 * Created on 13 sept. 2010, 19:29:39
 */

package application.frames;

import application.shapes.TCustomizedFrame;
import core.encryption.KeySetting;
import core.encryption.KeySettingsLoader;
import core.encryption.SteganosCryptor;
import java.awt.Graphics;
import java.awt.image.BufferedImage;
import java.io.File;
import javax.swing.JFileChooser;
import javax.swing.JFrame;
import lwcanvas.LWCanvas;
import lwcanvas.util.LWImgUtil;

/**
 *
 * @author Mitanjo
 */
public class SettingsFrame extends javax.swing.JDialog {
    private LWCanvas canvas;
    private BufferedImage keyImg;
    private boolean ok = false;
    private KeySetting keySetting;
    private JFrame parent;
    private TCustomizedFrame customizedFrame;

    public SettingsFrame(JFrame parent) {
        super(parent, true);

        this.parent = parent;

        initComponents();
        initFrame();        
    }

    private void initFrame() {
        setLocationRelativeTo(parent);

        keySetting = KeySettingsLoader.getInstance().loadSettings();
        keyImg = keySetting.getImgKey();
        txtStrKey.setText(keySetting.getStrKey());
        txtThresold.setText("" + keySetting.getThresold());
        txtDivision.setText("" + keySetting.getDivision());

        canvas = new LWCanvas();
        canvas.setBounds(0, 0, keyImgPanel.getBounds().width, keyImgPanel.getBounds().height);
        keyImgPanel.add(canvas);

        customizedFrame = new TCustomizedFrame(keyImg, 0, 0, keyImgPanel.getBounds().width, keyImgPanel.getBounds().height);
        customizedFrame.setGrid(keySetting.getDivision(), keySetting.getDivision());
        canvas.addShape(customizedFrame);
    }

    private void update() {
        keySetting.setDivision(Integer.parseInt(txtDivision.getText()));
        keySetting.setThresold(Integer.parseInt(txtThresold.getText()));
        keySetting.setStrKey(txtStrKey.getText());
        KeySettingsLoader.getInstance().saveSettings(keySetting);

        customizedFrame.setGrid(keySetting.getDivision(), keySetting.getDivision());
        customizedFrame.setImg(keyImg);

        canvas.repaint();
    }

    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jFileChooser1 = new javax.swing.JFileChooser();
        jPanel1 = new javax.swing.JPanel();
        keyImgPanel = new javax.swing.JPanel();
        txtBrowse = new javax.swing.JTextField();
        btnBrowse = new javax.swing.JButton();
        btnOK = new javax.swing.JButton();
        btnCancel2 = new javax.swing.JButton();
        lblStrKey = new javax.swing.JLabel();
        lblmgKey = new javax.swing.JLabel();
        txtDivision = new javax.swing.JTextField();
        lblDivision = new javax.swing.JLabel();
        txtStrKey = new javax.swing.JTextField();
        lblThresold = new javax.swing.JLabel();
        txtThresold = new javax.swing.JTextField();
        btnKeyBackup = new javax.swing.JButton();
        btnRefresh = new javax.swing.JButton();

        setDefaultCloseOperation(javax.swing.WindowConstants.DISPOSE_ON_CLOSE);
        setTitle("Security settings");
        setBackground(new java.awt.Color(0, 0, 0));
        setMinimumSize(new java.awt.Dimension(800, 600));
        addWindowListener(new java.awt.event.WindowAdapter() {
            public void windowClosing(java.awt.event.WindowEvent evt) {
                formWindowClosing(evt);
            }
        });

        jPanel1.setBackground(new java.awt.Color(0, 0, 0));
        jPanel1.setLayout(null);

        javax.swing.GroupLayout keyImgPanelLayout = new javax.swing.GroupLayout(keyImgPanel);
        keyImgPanel.setLayout(keyImgPanelLayout);
        keyImgPanelLayout.setHorizontalGroup(
            keyImgPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 300, Short.MAX_VALUE)
        );
        keyImgPanelLayout.setVerticalGroup(
            keyImgPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 300, Short.MAX_VALUE)
        );

        jPanel1.add(keyImgPanel);
        keyImgPanel.setBounds(20, 40, 300, 300);

        txtBrowse.setBackground(new java.awt.Color(0, 0, 0));
        txtBrowse.setEditable(false);
        jPanel1.add(txtBrowse);
        txtBrowse.setBounds(20, 360, 240, 30);

        btnBrowse.setBackground(java.awt.Color.green);
        btnBrowse.setFont(new java.awt.Font("Tahoma", 1, 11));
        btnBrowse.setText("...");
        btnBrowse.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnBrowseActionPerformed(evt);
            }
        });
        jPanel1.add(btnBrowse);
        btnBrowse.setBounds(270, 360, 50, 30);

        btnOK.setBackground(java.awt.Color.green);
        btnOK.setFont(new java.awt.Font("Tahoma", 1, 11));
        btnOK.setText("OK");
        btnOK.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnOKActionPerformed(evt);
            }
        });
        jPanel1.add(btnOK);
        btnOK.setBounds(450, 520, 90, 30);

        btnCancel2.setBackground(java.awt.Color.green);
        btnCancel2.setFont(new java.awt.Font("Tahoma", 1, 11));
        btnCancel2.setText("Cancel");
        btnCancel2.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnCancel2ActionPerformed(evt);
            }
        });
        jPanel1.add(btnCancel2);
        btnCancel2.setBounds(560, 520, 90, 30);

        lblStrKey.setFont(new java.awt.Font("Tahoma", 1, 11));
        lblStrKey.setForeground(java.awt.Color.green);
        lblStrKey.setText("String key");
        jPanel1.add(lblStrKey);
        lblStrKey.setBounds(350, 40, 90, 30);

        lblmgKey.setFont(new java.awt.Font("Tahoma", 1, 11));
        lblmgKey.setForeground(java.awt.Color.green);
        lblmgKey.setText("Image key");
        jPanel1.add(lblmgKey);
        lblmgKey.setBounds(20, 10, 120, 20);

        txtDivision.setBackground(new java.awt.Color(0, 0, 0));
        txtDivision.setForeground(java.awt.Color.green);
        jPanel1.add(txtDivision);
        txtDivision.setBounds(470, 140, 50, 30);

        lblDivision.setFont(new java.awt.Font("Tahoma", 1, 11));
        lblDivision.setForeground(java.awt.Color.green);
        lblDivision.setText("Division");
        jPanel1.add(lblDivision);
        lblDivision.setBounds(350, 140, 110, 30);

        txtStrKey.setBackground(new java.awt.Color(0, 0, 0));
        txtStrKey.setForeground(java.awt.Color.green);
        jPanel1.add(txtStrKey);
        txtStrKey.setBounds(470, 40, 280, 30);

        lblThresold.setFont(new java.awt.Font("Tahoma", 1, 11));
        lblThresold.setForeground(java.awt.Color.green);
        lblThresold.setText("Thresold (0~255)");
        jPanel1.add(lblThresold);
        lblThresold.setBounds(350, 90, 110, 30);

        txtThresold.setBackground(new java.awt.Color(0, 0, 0));
        txtThresold.setForeground(java.awt.Color.green);
        jPanel1.add(txtThresold);
        txtThresold.setBounds(470, 90, 50, 30);

        btnKeyBackup.setBackground(new java.awt.Color(255, 153, 0));
        btnKeyBackup.setFont(new java.awt.Font("Tahoma", 1, 11)); // NOI18N
        btnKeyBackup.setText("Backup key");
        btnKeyBackup.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnKeyBackupActionPerformed(evt);
            }
        });
        jPanel1.add(btnKeyBackup);
        btnKeyBackup.setBounds(670, 520, 110, 30);

        btnRefresh.setBackground(java.awt.Color.green);
        btnRefresh.setFont(new java.awt.Font("Tahoma", 1, 11));
        btnRefresh.setText("Refresh");
        btnRefresh.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnRefreshActionPerformed(evt);
            }
        });
        jPanel1.add(btnRefresh);
        btnRefresh.setBounds(350, 200, 90, 30);

        getContentPane().add(jPanel1, java.awt.BorderLayout.CENTER);

        pack();
    }// </editor-fold>//GEN-END:initComponents

    private void btnOKActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnOKActionPerformed
        ok = true;

        update();

        SteganosCryptor.getInstance().initialize(keySetting.getStrKey(), keySetting.getImgKey(), keySetting.getThresold(), keySetting.getDivision());

        dispose();
    }//GEN-LAST:event_btnOKActionPerformed

    private void btnCancel2ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnCancel2ActionPerformed
        dispose();
    }//GEN-LAST:event_btnCancel2ActionPerformed

    private void btnBrowseActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnBrowseActionPerformed
        jFileChooser1.setVisible(true);
        int res = jFileChooser1.showDialog(this, null);
        if(res == JFileChooser.APPROVE_OPTION) {
            File f = jFileChooser1.getSelectedFile();
            BufferedImage img = LWImgUtil.getInstance().filetoImage(f);
            txtBrowse.setText(jFileChooser1.getDescription(f));
            keySetting.setImgKey(img);
            keyImg = img;
            customizedFrame.setImg(img);
            canvas.repaint();
        }
    }//GEN-LAST:event_btnBrowseActionPerformed

    private void formWindowClosing(java.awt.event.WindowEvent evt) {//GEN-FIRST:event_formWindowClosing
        
    }//GEN-LAST:event_formWindowClosing

    private void btnRefreshActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnRefreshActionPerformed
        update();
    }//GEN-LAST:event_btnRefreshActionPerformed

    private void btnKeyBackupActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnKeyBackupActionPerformed
        KeySettingsLoader.getInstance().createBackup();
    }//GEN-LAST:event_btnKeyBackupActionPerformed

////    public static void main(String args[]) {
////        java.awt.EventQueue.invokeLater(new Runnable() {
////            public void run() {
////                SettingsFrame dialog = new SettingsFrame(new javax.swing.JFrame());
////                dialog.addWindowListener(new java.awt.event.WindowAdapter() {
////                    public void windowClosing(java.awt.event.WindowEvent e) {
////                        System.exit(0);
////                    }
////                });
////                dialog.setVisible(true);
////            }
////        });
////    }

    public boolean isOk() {
        return ok;
    }

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton btnBrowse;
    private javax.swing.JButton btnCancel2;
    private javax.swing.JButton btnKeyBackup;
    private javax.swing.JButton btnOK;
    private javax.swing.JButton btnRefresh;
    private javax.swing.JFileChooser jFileChooser1;
    private javax.swing.JPanel jPanel1;
    private javax.swing.JPanel keyImgPanel;
    private javax.swing.JLabel lblDivision;
    private javax.swing.JLabel lblStrKey;
    private javax.swing.JLabel lblThresold;
    private javax.swing.JLabel lblmgKey;
    private javax.swing.JTextField txtBrowse;
    private javax.swing.JTextField txtDivision;
    private javax.swing.JTextField txtStrKey;
    private javax.swing.JTextField txtThresold;
    // End of variables declaration//GEN-END:variables

}
