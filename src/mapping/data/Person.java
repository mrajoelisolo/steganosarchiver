package mapping.data;

import core.encryption.SteganosCryptor;
import lwdatabase.IEntity;
import mapping.profile.Profile;

/**
 *
 * @author Mitanjo
 */
public class Person implements IEntity {
    private Long id;
    private String firstName;
    private String lastName;
    private String birthDate;
    private String address;
    private String contact;
    private String description;

    private Profile profile;

    public Person() {}

    public static final String ID_PROFIL_COL = "id";
    public Long getId() {
        return id;
    }
    public void setId(Long id) {
        this.id = id;
    }

    public static final String FIRSTNAME_COL = "firstName";
    public String getFirstName() {
        if(firstName == null) return "";
        return firstName;
    }
    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public static final String LASTNAME_COL = "lastName";
    public String getLastName() {
        if(lastName == null) return "";
        return lastName;
    }
    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public static final String BIRTHDATE_COL = "birthDate";
    public String getBirthDate() {
        if(birthDate == null) return "";
        return birthDate;
    }
    public void setBirthDate(String birthDate) {
        this.birthDate = birthDate;
    }

    public static final String ADDRESS_COL = "address";
    public String getAddress() {
        if(address == null) return "";
        return address;
    }
    public void setAddress(String address) {
        this.address = address;
    }

    public static final String CONTACT_COL = "contact";
    public String getContact() {
        if(contact == null) return "";
        return contact;
    }
    public void setContact(String contact) {
        this.contact = contact;
    }

    public static final String DESCRIPTION_COL = "description";
    public String getDescription() {
        if(description == null) return "";
        return description;
    }
    public void setDescription(String description) {
        this.description = description;
    }

    public Profile getProfile() {
        return profile;
    }

    public void setProfile(Profile profile) {
        this.profile = profile;
    }

    public static final String SZFIRSTNAME_COL = "szFirstName";
    public String getSzFirstName() {
        return SteganosCryptor.getInstance().decrypt(getFirstName());
    }
    public void setSzFirstName(String firstName) {
        this.firstName = SteganosCryptor.getInstance().encrypt(firstName);
    }

    public static final String SZLASTNAME_COL = "szLastName";
    public String getSzLastName() {
        return SteganosCryptor.getInstance().decrypt(getLastName());
    }
    public void setSzLastName(String lastName) {
        this.lastName = SteganosCryptor.getInstance().encrypt(lastName);
    }

    public static final String SZBIRTHDATE_COL = "szBirthDate";
    public String getSzBirthDate() {
        return SteganosCryptor.getInstance().decrypt(getBirthDate());
    }
    public void setSzBirthDate(String birthDate) {
        this.birthDate = SteganosCryptor.getInstance().encrypt(birthDate);
    }

    public static final String SZADDRESS_COL = "szAddress";
    public String getSzAddress() {
        return SteganosCryptor.getInstance().decrypt(getAddress());
    }
    public void setSzAddress(String address) {
        this.address = SteganosCryptor.getInstance().encrypt(address);
    }

    public static final String SZCONTACT_COL = "szContact";
    public String getSzContact() {
        return SteganosCryptor.getInstance().decrypt(getContact());
    }
    public void setSzContact(String contact) {
        this.contact = SteganosCryptor.getInstance().encrypt(contact);
    }

    public static final String SZDESCRIPTION_COL = "szDescription";
    public String getSzDescription() {
        return SteganosCryptor.getInstance().decrypt(getDescription());
    }
    public void setSzDescription(String description) {
        this.description = SteganosCryptor.getInstance().encrypt(description);
    }
}
