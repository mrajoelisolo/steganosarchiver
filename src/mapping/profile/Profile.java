package mapping.profile;

import java.util.Set;
import lwdatabase.IEntity;
import lwutil.LWCryptor;
import mapping.data.Person;

/**
 *
 * @author Mitanjo
 */
public class Profile implements IEntity {
    private Long id;
    private String login;
    private String pwd;
    private int privilege;

    private Set<Person> persons;

    public static final String ID_PROFIL_COL = "id";
    public Long getId() {
        return id;
    }
    public void setId(Long idProfile) {
        this.id = idProfile;
    }

    public static final String LOGIN_COL = "login";
    public String getLogin() {
        return login;
    }
    public void setLogin(String login) {        
        this.login = login;
    }

    public static final String PWD_COL = "pwd";
    public String getPwd() {
        return pwd;
    }
    public void setPwd(String pwd) {
        this.pwd = pwd;
    }

    public static final String PRIVILEGE_COL = "privilege";
    public int getPrivilege() {
        return privilege;
    }
    public void setPrivilege(int privilege) {
        this.privilege = privilege;
    }

    public static final String SZPRIVILEGE_COL = "szPrivilege";
    public String getSzPrivilege() {
        if(privilege == 1) return "ADMIN";
        else if (privilege == 2) return "USER";
        return "";
    }

    public static final String SZLOGIN_COL = "szLogin";
    public String getSzLogin() {
        return LWCryptor.getInstance().decrypt(login);
    }
    public void setSzLogin(String login) {
        this.login = LWCryptor.getInstance().encrypt(login);
    }

    public static final String SZPWD_COL = "szPwd";
    public String getSzPwd() {
        return LWCryptor.getInstance().decrypt(pwd);
    }
    public void setSzPwd(String pwd) {
        this.pwd = LWCryptor.getInstance().encrypt(pwd);
    }

    public Set getPersons() {
        return persons;
    }

    public void setPersons(Set<Person> persons) {
        this.persons = persons;
    }
}
