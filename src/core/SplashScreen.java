package core;

import application.shapes.TIconMangekyo;
import core.tools.FrameUtilities;
import application.frames.MenuFrame;
import java.awt.Graphics;
import java.awt.Image;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.JFrame;
import javax.swing.WindowConstants;
import lwcanvas.LWCanvas;
import lwutil.LWResourceLoader;

/**
 *
 * @author Mitanjo
 */
public class SplashScreen extends JFrame implements Runnable {
    private Thread myThread;
    private Thread tCounter;
    private int count = 0;

    private LWCanvas canvas;
    private Image bcgImg = LWResourceLoader.getInstance().loadImage("img/splash/splash.jpg");
    private TIconMangekyo iconMangekyo;

    public SplashScreen() {
        initComponents();

        myThread = new Thread(this);
        myThread.start();

        tCounter = new Thread(this);
        tCounter.start();
    }

    private void initComponents() {
        setUndecorated(true);
        setSize(900, 450);
        setLocationRelativeTo(this);
        setDefaultCloseOperation(WindowConstants.EXIT_ON_CLOSE);
        getContentPane().setLayout(null);

        canvas = new LWCanvas() {
            @Override
            public void paintBackground(Graphics g) {
               if(bcgImg != null)
                   g.drawImage(bcgImg, 0, 0, getBounds().width, getBounds().height, null);
            }
        };
        canvas.setLocation(0, 0);
        canvas.setSize(getBounds().width, getBounds().height);
        canvas.setAntialiasing(LWCanvas.ANTIALIASING_ON);
        canvas.setRenderQuality(LWCanvas.RENDER_HIGH_QUALITY);
        getContentPane().add(canvas);

        iconMangekyo = new TIconMangekyo(900, 300);
        iconMangekyo.setAlphaValue(0.2f);
        canvas.addShape(iconMangekyo);
    }

    private void core() {
        iconMangekyo.turn(0.05f);

        canvas.repaint();
    }

    public void run() {
        while(count < 4) {
            try {
                if(Thread.currentThread().equals(myThread)) {
                    Thread.sleep(30);
                    core();
                }else if(Thread.currentThread().equals(tCounter)) {
                    Thread.sleep(3000);
                    count++;

                    if(count > 3)
                        FrameUtilities.getInstance().show(new MenuFrame());
                }
            } catch (InterruptedException ex) {
                Logger.getLogger(SplashScreen.class.getName()).log(Level.SEVERE, null, ex);
            }
        }

        dispose();        
    }  
}
