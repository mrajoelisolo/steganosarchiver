package core.markrecon;

import java.awt.image.BufferedImage;
import core.image.GrayScaleAnalyzer;

/**
 *
 * @author Mitanjo
 */
public class ReconModuleOne implements IReconModule {
    private float acceptablePercentage;
    private int minGrayscale;

    public ReconModuleOne(float acceptablePercentage, int minGrayscale) {
        this.acceptablePercentage = acceptablePercentage;
        this.minGrayscale = minGrayscale;
    }

    public boolean recon(BufferedImage img) {
        boolean res = false;

        float grayScalePercentage = GrayScaleAnalyzer.getInstance().grayScalePercentage(img, getMinGrayscale());
        if(grayScalePercentage >= getAcceptablePercentage()) res = true;

        return res;
    }

    public int recon2(BufferedImage img) {
        int res = 0;

        float grayScalePercentage = GrayScaleAnalyzer.getInstance().grayScalePercentage(img, getMinGrayscale());
        if(grayScalePercentage >= getAcceptablePercentage()) res = 1;

        return res;
    }

    public float getAcceptablePercentage() {
        return acceptablePercentage;
    }

    public void setAcceptablePercentage(float acceptablePercentage) {
        this.acceptablePercentage = acceptablePercentage;
    }

    public int getMinGrayscale() {
        return minGrayscale;
    }

    public void setMinGrayscale(int minGrayscale) {
        this.minGrayscale = minGrayscale;
    }
}
