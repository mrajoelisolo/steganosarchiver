package core.markrecon;

import java.awt.image.BufferedImage;

/**
 *
 * @author Mitanjo
 */
public interface IReconModule {
    boolean recon(BufferedImage img);
}
