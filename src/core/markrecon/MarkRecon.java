package core.markrecon;

import java.awt.image.BufferedImage;

/**
 *
 * @author Mitanjo
 */
public class MarkRecon {
    private static MarkRecon instance = new MarkRecon();

    private MarkRecon() {}

    public static MarkRecon getInstance() {
        return instance;
    }

    public boolean isChecked(BufferedImage img, IReconModule module) {
        if(img == null || module == null) return false;

        boolean res = false;

        res = module.recon(img);

        return res;
    }
}
