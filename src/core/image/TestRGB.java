package core.image;

import java.awt.image.BufferedImage;
import lwcanvas.util.LWImgUtil;
import lwutil.LWResourceLoader;

/**
 *
 * @author Mitanjo
 */
public class TestRGB {
    public static void main(String[] args) {
        BufferedImage img = LWImgUtil.getInstance().toBufferedImage(LWResourceLoader.getInstance().loadImage("img/Acquisition.jpg"));

        RGBModel m = PixelAnalyzer.getInstance().getPixel(img, 135, 86);
        int alpha = m.getAlpha();
        int red = m.getRed();
        int green = m.getGreen();
        int blue = m.getBlue();

        System.out.println("r=" + red + " g=" + green + " b=" + blue + " alpha=" + alpha);
    }
}
