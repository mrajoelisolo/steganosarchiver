package core.image;

import core.image.RGBModel;
import core.image.PixelAnalyzer;
import java.awt.image.BufferedImage;

/**
 *
 * @author Mitanjo
 * Cette classe a poure responsabilité de caluler le taux de gris
 */
public class GrayScaleAnalyzer {
    private static GrayScaleAnalyzer instance = new GrayScaleAnalyzer();

    private GrayScaleAnalyzer() {}

    public static GrayScaleAnalyzer getInstance() {
        return instance;
    }

    public float grayScalePercentage(BufferedImage img, int min) {
        if(img == null) return 0f;

        int wdt = img.getWidth();
        int hgt = img.getHeight();
        float count = 0;

        if(wdt == 0 || hgt == 0) return 0f;

        for(int y = 0; y < hgt; y++) {
            for(int x = 0; x < wdt; x++) {
                RGBModel rgb = PixelAnalyzer.getInstance().getPixel(img, x, y);
        
                int red = rgb.getRed();
                int green = rgb.getGreen();
                int blue = rgb.getBlue();

                if(red <= min && green <= min && blue <= min)
                    count++;
            }
        }

        return (count / wdt / hgt) * 100;
    }
}