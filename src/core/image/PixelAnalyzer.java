package core.image;

import java.awt.image.BufferedImage;

/**
 *
 * @author Mitanjo
 * @since 19/06/10
 * Cette classe a pour responsabilité d'obtenir le modèle de couleur d'un pixel
 * a savoir le composant rouge, vert, bleu et alpha
 */
public class PixelAnalyzer {
    private static PixelAnalyzer instance = new PixelAnalyzer();

    private PixelAnalyzer() {}

    public static PixelAnalyzer getInstance() {
        return instance;
    }

    private RGBModel rgbModel = new RGBModel();
    public RGBModel getPixel(BufferedImage img, int x, int y) {
        int rgb = img.getRGB(x, y);
        int alpha = (rgb >> 24) & 0xff;
        int red = (rgb >> 16) & 0xff;
        int green = (rgb >> 8) & 0xff;
        int blue = rgb & 0xff;

        rgbModel.setAlpha(alpha);
        rgbModel.setBlue(blue);
        rgbModel.setGreen(green);
        rgbModel.setRed(red);

        return rgbModel;
    }
}
