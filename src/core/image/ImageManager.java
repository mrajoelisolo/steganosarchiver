package core.image;

import java.awt.color.ColorSpace;
import java.awt.image.BufferedImage;
import java.awt.image.ColorConvertOp;

/**
 *
 * @author Mitanjo
 */
public class ImageManager {
    private static ImageManager instance = new ImageManager();

    public ImageManager() {}

    public static ImageManager getInstance() {
        return instance;
    }

    public BufferedImage ToGrayScale(BufferedImage img) {
        BufferedImage res = null;
        
        try {
            ColorSpace gray = ColorSpace.getInstance(ColorSpace.CS_GRAY);
            ColorSpace color = ColorSpace.getInstance(ColorSpace.CS_sRGB);
            ColorConvertOp ccOp = new ColorConvertOp(color, gray, null);
            res = ccOp.filter(img, null);
        }catch(Exception e) {
            
        }

        return res;
    }
}
