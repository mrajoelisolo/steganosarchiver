package core.encryption;

import core.image.GrayScaleAnalyzer;
import java.awt.image.BufferedImage;

/**
 *
 * @author Mitanjo
 */
public class KeyFactory {
    private static KeyFactory instance = new KeyFactory();

    private KeyFactory() {}

    public static KeyFactory getInstance() {
        return instance;
    }

    public int[][] createIMatrix(BufferedImage img, int thresold, int n) {
        int res[][] = new int[n][n];

        int wdt = img.getWidth();
        int hgt = img.getHeight();
        int x = 0;
        int y = 0;

        for(int i = 0; i < n; i++) {
            x = 0;
            for(int j = 0; j < n; j++) {
                BufferedImage subImg = img.getSubimage(x, y, wdt/n, hgt/n);
                res[i][j] = (int)GrayScaleAnalyzer.getInstance().grayScalePercentage(subImg, thresold);

                x += wdt/n;
            }
            y += hgt/n;
        }

        return res;
    }
}
