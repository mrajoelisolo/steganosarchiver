package core.encryption;

import java.io.*;
import javax.swing.JOptionPane;

public class KeySettingsLoader {
    //Singleton Pattern
    private static KeySettingsLoader instance;

    private KeySettingsLoader() {}

    public static KeySettingsLoader getInstance() {
        if(instance == null)
            instance = new KeySettingsLoader();
        return instance;
    }

    private String path = "settings/param.stg";

    public KeySetting loadSettings() {
        KeySetting res = null;

        try {
            FileInputStream fis = new FileInputStream(path);
            ObjectInputStream ois = new ObjectInputStream(fis);
            res = (KeySetting) ois.readObject();
            current = res;
        }catch(IOException ex) {
            ex.printStackTrace();
        }catch(ClassNotFoundException e) {
            e.printStackTrace();
        }

        return res;
    }

    public void saveSettings(KeySetting keySetting) {
        try {
            FileOutputStream fos = new FileOutputStream(path);
            ObjectOutputStream oos = new ObjectOutputStream(fos);
            oos.writeObject(keySetting);
            oos.flush();
            oos.close();
        }catch(IOException e) {
            e.printStackTrace();
        }
    }

    public void saveSettings(KeySetting keySetting, String path) {
        try {
            FileOutputStream fos = new FileOutputStream(path);
            ObjectOutputStream oos = new ObjectOutputStream(fos);
            oos.writeObject(keySetting);
            oos.flush();
            oos.close();
        }catch(IOException e) {
            e.printStackTrace();
        }
    }

    private KeySetting current = null;
    public void createBackup() {
        if(current != null) {
            saveSettings(current, "settings/backup.stg");
            JOptionPane.showMessageDialog(null, "A backup of the settings is saved on settings\\backup.stg", "", JOptionPane.INFORMATION_MESSAGE);
        }
    }
}
