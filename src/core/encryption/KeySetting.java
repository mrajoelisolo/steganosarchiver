package core.encryption;

import java.awt.image.BufferedImage;
import java.io.Serializable;
import lwcanvas.util.LWImgUtil;

/**
 *
 * @author Mitanjo
 */
public class KeySetting implements Serializable {
    private String strKey;
    private byte[] imgKey;
    private int thresold;
    private int division;

    public String getStrKey() {
        return strKey;
    }

    public void setStrKey(String strKey) {
        this.strKey = strKey;
    }

    public BufferedImage getImgKey() {
        return LWImgUtil.getInstance().bytesToImg(imgKey);
    }

    public void setImgKey(BufferedImage imgKey) {
        this.imgKey = LWImgUtil.getInstance().imgToBytes(imgKey);
    }

    public int getThresold() {
        return thresold;
    }

    public void setThresold(int thresold) {
        this.thresold = thresold;
    }

    public int getDivision() {
        return division;
    }

    public void setDivision(int division) {
        this.division = division;
    }
}
