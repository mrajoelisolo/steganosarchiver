package core.encryption;

import core.math.Arithmetic;
import java.awt.Image;
import java.awt.image.BufferedImage;
import lwcanvas.util.LWImgUtil;

/**
 *
 * @author Mitanjo
 */
public class SteganosCryptorBeta {
    private static SteganosCryptorBeta instance = new SteganosCryptorBeta();

    private SteganosCryptorBeta() {}

    public static SteganosCryptorBeta getInstance() {
        return instance;
    }

    private int[][] keyMatrix;
    private int[][] supplyMatrix;
    private BufferedImage keyImage;

    public void initialize(Image keyImage, int thresold, int n) {
        this.keyImage = LWImgUtil.getInstance().toBufferedImage(keyImage);
        keyMatrix = KeyFactory.getInstance().createIMatrix(this.keyImage, thresold, n);
        int avg = Arithmetic.getInstance().getAverageValue(keyMatrix);
        Arithmetic.getInstance().fillZeroWithAlternativeSequence(keyMatrix);

        supplyMatrix = new int[n][n];
        for(int i = 0; i < n; i++) {
            for(int j = 0; j < n; j++) {
                if(keyMatrix[i][j]%2 == 0)
                    supplyMatrix[i][j] = 1;
                else
                    supplyMatrix[i][j] = -1;
            }
        }

        for(int i = 0; i < n; i++) {
            for(int j = 0; j < n; j++) {
                if(keyMatrix[i][j] > avg)
                    supplyMatrix[i][j] *= 1;
                else
                    supplyMatrix[i][j] *= -1;
            }
        } 
    }    

    public String encrypt(String str) {
        String res = "";

        int i = 0;
        int j = 0;
        int count = 0;

        while(count < str.length()) {
            for(int row = 0; row < keyMatrix.length && i < str.length(); row++) {
                for(int col = 0; col < keyMatrix[0].length && i < str.length(); col++) {
                    char s = str.charAt(i);

                    int km = keyMatrix[row][row];

                    char chr;

                    if(159 < s && s < 256)
                        chr = s;
                    else if(supplyMatrix[row][col] == -1)
                        chr = StringUtilities.getInstance().splitStdChar(s, km);
                    else
                        chr = StringUtilities.getInstance().splitStdChar(s, -km);

                    res += chr;

                    i++;
                    j++;

                    count++;
                }
            }
        }


        return res;
    }

    public String decrypt(String str) {
        String res = "";

        int i = 0;
        int j = 0;
        int count = 0;

        while(count < str.length()) {
            for(int row = 0; row < keyMatrix.length && i < str.length(); row++) {
                for(int col = 0; col < keyMatrix[0].length && i < str.length(); col++) {
                    char s = str.charAt(i);

                    int km = keyMatrix[row][row];

                    char chr;

                    if(159 < s && s < 256)
                        chr = s;
                    else if(supplyMatrix[row][col] == -1)
                        chr = StringUtilities.getInstance().splitStdChar(s, -km);
                    else
                        chr = StringUtilities.getInstance().splitStdChar(s, km);

                    res += chr;

                    i++;
                    j++;

                    count++;
                }
            }
        }


        return res;
    }
}
