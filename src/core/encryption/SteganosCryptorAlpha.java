package core.encryption;

/**
 *
 * @author Mitanjo
 */
public class SteganosCryptorAlpha {
    private static SteganosCryptorAlpha instance = new SteganosCryptorAlpha();

    private SteganosCryptorAlpha() {}

    public static SteganosCryptorAlpha getInstance() {
        return instance;
    }

    private String key = "";

    public void initialize(String key) {
        this.key = key;
    }

    public String encrypt(String str) {
        String res = "";

        String keys = breadKeyWithoutSpacing(str, key);

        for(int i = 0; i < str.length(); i++) {
            char c = str.charAt(i);
            char k = keys.charAt(i);

            char chr;
            if(159 < c && c < 256)
                chr = StringUtilities.getInstance().splitExtChar(c, k);
            else
                chr = StringUtilities.getInstance().splitStdChar(c, k);

            res += chr;
        }

        return res;
    }

    public String decrypt(String str) {
        String res = "";

        String keys = breadKeyWithoutSpacing(str, key);

        for(int i = 0; i < str.length(); i++) {
            char c = str.charAt(i);
            char k = keys.charAt(i);

            char chr;
            if(159 < c && c < 256)
                chr = StringUtilities.getInstance().splitExtChar(c, -k);
            else
                chr = StringUtilities.getInstance().splitStdChar(c, -k);

            res += chr;
        }

        return res;
    }    

    private String breadKeyWithSpacing(String str, String keySeq) {
        String res = "";

        int count = 0;
        
        for(int i = 0; i < str.length(); i++) {
            char c = str.charAt(i);
            
            if(c != ' ') {
                res += keySeq.charAt(count);
                count++;

                if(count > keySeq.length() - 1)
                    count = 0;
            }else
                res += " ";
        }

        return res;
    }

    private String breadKeyWithoutSpacing(String str, String keySeq) {
        String res = "";

        int count = 0;

        for(int i = 0; i < str.length(); i++) {
            res += keySeq.charAt(count);
            count++;

            if(count > keySeq.length() - 1)
                    count = 0;
        }

        return res;
    }
}
