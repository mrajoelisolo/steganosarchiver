package core.encryption;

import java.awt.image.BufferedImage;

/**
 *
 * @author Mitanjo
 */
public class SteganosCryptor {
    private static SteganosCryptor instance = new SteganosCryptor();

    private SteganosCryptor() {}

    public static SteganosCryptor getInstance() {
        return instance;
    }

    public void initialize(String key1, BufferedImage keyImg, int thresold, int n) {
        SteganosCryptorAlpha.getInstance().initialize(key1);
        SteganosCryptorBeta.getInstance().initialize(keyImg, thresold, n);
    }

    public String encrypt(String str) {
        String res = str;
        
        res = SteganosCryptorAlpha.getInstance().encrypt(res);
        res = SteganosCryptorBeta.getInstance().encrypt(res);

        return res;
    }

    public String decrypt(String str) {
        String res = str;

        res = SteganosCryptorBeta.getInstance().decrypt(res);
        res = SteganosCryptorAlpha.getInstance().decrypt(res);

        return res;
    }
}
