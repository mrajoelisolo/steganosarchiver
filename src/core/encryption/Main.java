package core.encryption;

import core.math.Arithmetic;
import java.awt.image.BufferedImage;
import lwcanvas.util.LWImgUtil;
import lwutil.LWResourceLoader;

/**
 *
 * @author Mitanjo
 */
public class Main {
    public static void main(String[] args) {
        //Tester si java peut récupérér un caractère après l'avoir transformé en caractère de type \u0000
//        String str = "a";
//        System.out.println("a : " + str.codePointAt(0));
//
//        char c = '*';
//        int k = 128;
//
//        char c2 = getSplittedChar(c, k);
//        System.out.println("" + c2);
//
//        char c3 = getSplittedChar(c2, -k);
//        System.out.println("" + c3);

//        String str = "Narutouzumaki";
//        System.out.println(str);
//        String res = Genjutsu.getInstance().encrypt(str);
//        System.out.println(res);
//
//        String str2 = "Ā";
//        System.out.println("" + str2.codePointAt(0));
//
//        char a = 'z';
//
//        //System.out.println(StringUtilities.getInstance().getSplittedChar(a, -95));
//        System.out.println(String.valueOf((int)(' ' - 'z')));

//        int x = 5;
//        int k = -30;
//        int a = 20 - (-(x + k)%20);
//        System.out.println("" + a);

//        char c = 'a';
//        int k = 94;
//        char res = StringUtilities.getInstance().splitStdChar(c, k);
//        System.out.println("" + res);

//        String str = "I love hinata";
//        String str2 = Genjutsu.getInstance().encrypt(str);
//        String str3 = Genjutsu.getInstance().decrypt(str2);
//
//        System.out.println(str);
//        System.out.println(str2);
//        System.out.println(str3);

//        char init = 'a';
//        char a = StringUtilities.getInstance().splitStdChar(init, 95);
//        char b = StringUtilities.getInstance().splitStdChar(a, -95);
//        System.out.println(String.valueOf((int)a));
//        System.out.println(String.valueOf((int)b));
//        System.out.println("" + init);
//        System.out.println("" + a);
//        System.out.println("" + b);

//        String str = "I love hinata";
//        String str2 = SteganosCoderAlpha.getInstance().encrypt(str);
//        String str3 = SteganosCoderAlpha.getInstance().decrypt(str2);
//        for(int i = 0; i < str.length(); i++) {
//            str2 += StringUtilities.getInstance().splitStdChar(str.charAt(i), k);
//        }
//
//        for(int i = 0; i < str.length(); i++) {
//            str3 += StringUtilities.getInstance().splitStdChar(str2.charAt(i), -k);
//        }
//        System.out.println(str);
//        System.out.println(str2);
//        System.out.println(str3);

        //int n = 5;
        //BufferedImage img = LWImgUtil.getInstance().toBufferedImage(LWResourceLoader.getInstance().loadImage("img/key/key.jpg"));
//        int[][] m = KeyFactory.getInstance().createIMatrix(img, 100, n);
//
//        Arithmetic.getInstance().fillZeroWithAlternativeSequence(m);
//        int avg = Arithmetic.getInstance().getAverageValue(m);
//        System.out.println("" + avg);
//
//        show(m);
//
//        int[][] m2 = new int[n][n];
//        for(int i = 0; i < n; i++) {
//            for(int j = 0; j < n; j++) {
//                if(m[i][j]%2 == 0)
//                    m2[i][j] = 1;
//                else
//                    m2[i][j] = -1;
//            }
//        }
//        System.out.println();
//        show(m2);
//
//        int[][] m3 = new int[n][n];
//        for(int i = 0; i < n; i++) {
//            for(int j = 0; j < n; j++) {
//                if(m[i][j] > avg)
//                    m3[i][j] = 1;
//                else
//                    m3[i][j] = -1;
//            }
//        }
//        System.out.println();
//        show(m3);
//
//        int[][] m4 = new int[n][n];
//        for(int i = 0; i < n; i++) {
//            for(int j = 0; j < n; j++) {
//                m4[i][j] = m2[i][j] * m3[i][j];
//            }
//        }
//        System.out.println();
//        show(m4);

//        BufferedImage imgKey = LWImgUtil.getInstance().toBufferedImage(LWResourceLoader.getInstance().loadImage("img/key/key.jpg"));
//        int thresold = 100;
//        int n = 5;
//
//        SteganosCoderBeta.getInstance().initialize("c3df32ea", imgKey, thresold, n);
//
//        String str = "";
//        for(int i = 32; i < 127; i++) {
//            str += String.valueOf((char)i);
//        }
//
//        str = "I love hinata";
//
//        System.out.println(str);
//
//        int k = 'a';
//        String res = "";
//        for(char c : str.toCharArray()) {
//            res += StringUtilities.getInstance().splitStdChar(c, k);
//        }
//
//        System.out.println();
//        System.out.println(res);
//
//        String res2 = "";
//        for(char c : res.toCharArray()) {
//            char chr = StringUtilities.getInstance().splitStdChar(c, -k);
//            res2 += chr;
//            //System.out.println(String.valueOf((int)chr));
//        }
//        System.out.println();
//        System.out.println(res2);

//        System.out.println("" + StringUtilities.getInstance().splitStdChar('b', -1));
//        char c = 'a';
//        char d = StringUtilities.getInstance().splitStdChar(c, 95);
//        char e = StringUtilities.getInstance().splitStdChar(d, 1);
//
//        System.out.println("" + c);
//        System.out.println("" + d);
//        System.out.println("" + e);

        for(int i = 160; i < 256; i++) {
            System.out.print("" + String.valueOf((char)i));
        }
        System.out.println("");
        System.out.print("" + String.valueOf((int)' '));
    }

    private static void show(int[][] m) {
        for(int i = 0; i < m.length; i++) {
            for(int j = 0; j < m[0].length; j++) {
                System.out.print("" + m[i][j] + "  ");
            }
            System.out.println();
        }
    }

    private static char getSplittedChar(char c, int k) {
        try {
            return String.valueOf((char)(c + k)).charAt(0);
        }catch(Exception e) {
            return c;
        }
    }
}
