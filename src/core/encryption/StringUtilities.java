package core.encryption;

/**
 *
 * @author Mitanjo
 */
public class StringUtilities {
    private static StringUtilities instance = new StringUtilities();

    private StringUtilities() {}

    public static StringUtilities getInstance() {
        return instance;
    }

    public char splitStdChar(char c, int vk) {
        try {
            char len = '~' - ' ' + 1;

            int k = vk%len;

            if(c == '\n' || c == '\b' || c == '\t' || c == '\f' || c == '\r') return c;            

            if(c + k > '~')
                return (char)((c - ' ' + k)%len + ' ');
            else if(c + k < ' ') {
                //chr = (char)(len - (Math.abs(c + k - ' ')%len) + ' ');
                return (char)('~' - (Math.abs(k) - c + ' ') + 1);
            }else if(c + k == ' ')
                return ' ';
            else
                return (char)(c + k);
        }catch(Exception e) {
            e.printStackTrace();
            return c;
        }
    }

    public char splitExtChar(char c, int vk) {
        try {
            char len = 'ÿ' - ' ' + 1;

            int k = vk%len;

            if(c == '\n' || c == '\b' || c == '\t' || c == '\f' || c == '\r') return c;

            if(c + k > 'ÿ')
                return (char)((c - ' ' + k)%len + ' ');
            else if(c + k < ' ') {
                return (char)('ÿ' - (Math.abs(k) - c + ' ') + 1);
            }else if(c + k == ' ')
                return ' ';
            else
                return (char)(c + k);
        }catch(Exception e) {
            e.printStackTrace();
            return c;
        }
    }

    public char unsafeSplitChar(char c, int k) {
        try {            
            return (char)(c + k);
        }catch(Exception e) {
            e.printStackTrace();
            return c;
        }
    }
}
