package core.math;

/**
 *
 * @author Mitanjo
 */
public class Arithmetic {
   private static Arithmetic instance = new Arithmetic();

    private Arithmetic() {}

    public static Arithmetic getInstance() {
        return instance;
    }

    public int pgcd(int a, int b) {
        if(a == 0 || b == 0) return 0;
        if(a > b) return pgcd(a-b, b);
        else if(a < b) return pgcd(b, a);
        else return a;
    }

    public int ppcm(int a, int b) {
        return a*b/pgcd(a, b);
    }

    public void fillZeroWithAlternativeSequence(int[][] m) {
        int a = 1;
        int b = m.length*m[0].length;
        boolean omote = true;

        for(int i = 0; i < m.length; i++)
            for(int j = 0; j < m[0].length; j++)
                if(m[i][j] == 0) {
                    if(omote)
                        m[i][j] = a++;
                    else
                        m[i][j] = b--;

                    omote = !omote;
                }
    }

    public int getAverageValue(int[][] m) {
        int res = 0;

        int rows = m.length;
        int cols = m[0].length;

        for(int i = 0; i < rows; i++)
            for(int j = 0; j < cols; j++)
                res += m[i][j];

        res = res/(rows*cols);

        return res;
    }
}
