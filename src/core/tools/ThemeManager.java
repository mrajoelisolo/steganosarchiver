package core.tools;

import java.awt.Color;
import javax.swing.JTextField;
import lwcanvas.util.LWColorProvider;
import lwcanvas.util.LWFontProvider;

/**
 *
 * @author Mitanjo
 */
public class ThemeManager {
    private static ThemeManager instance = new ThemeManager();

    private ThemeManager() {}

    public static ThemeManager getInstance() {
        if(instance == null)
            instance = new ThemeManager();

        return instance;
    }

    public void setTextFieldToCurrentTheme(JTextField tf) {
        tf.setBackground(Color.BLACK);
        tf.setForeground(Color.GREEN);
        tf.setFont(LWFontProvider.BTN_FONT_COURIER_NORMAL_20);
        tf.setSelectedTextColor(Color.GREEN);
        tf.setSelectionColor(LWColorProvider.getInstance().getColor(0, 64, 0));
    }
}
