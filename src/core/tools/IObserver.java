package core.tools;

/**
 *
 * @author Mitanjo
 */
public interface IObserver {
    void notify(Object o);
}
