package core;

import application.util.ProfileManager;
import hibernate.cfg.HibernateUtil;
import java.util.Date;
import mapping.data.Person;
import org.hibernate.Transaction;
import mapping.profile.*;
import org.hibernate.HibernateException;
import org.hibernate.Session;

/**
 *
 * @author Mitanjo
 */
public class TestDB {
    public static void main(String[] args) {
        Session session = HibernateUtil.getSessionFactory().getCurrentSession();
        Transaction tx = null;

        try {
            tx = session.beginTransaction();

//            Profile p = new Profile();
//            p.setLogin("Naruto");
//            p.setPwd("ttebayo");
//            p.setPrivilege(1);
//            session.save(p);

//            Profile p = (Profile) session.load(Profile.class, 1l);
//            Person per = new Person();
//            per.setFirstName("Jack");
//            per.setLastName("Kirby");
//            per.setBirthDate(new Date());
//            per.setContact("kirby.com");
//            per.setDescription("Huh");
//            per.setProfile(p);
//
//            session.save(per);

//            Profile p = (Profile) session.load(Profile.class, 1l);
//            p.getPersons();

//            Person per = (Person) session.load(Person.class, 1l);
//            System.out.println(per.getFirstName());
//            Profile p = per.getProfile();
//            System.out.println(p.getLogin());

//            Profile pr = (Profile) session.load(Profile.class, 1l);
//            pr.setSzLogin("naruto");
//            pr.setSzPwd("uzumaki");
//            session.update(pr);

//            System.out.println(pr.getSzLogin());
//            System.out.println(pr.getSzPwd());

            Profile pr = ProfileManager.getInstance().getProfile("naruto", "uzumaki");
            if(pr != null)
                System.out.println(pr.getSzLogin() + " " + pr.getSzPwd());

            tx.commit();
        }catch(HibernateException e) {
           e.printStackTrace();
        }
    }
}
