package core;

import hibernate.cfg.HibernateUtil;
import mapping.profile.Profile;
import org.hibernate.HibernateException;
import org.hibernate.Session;
import org.hibernate.Transaction;

/**
 *
 * @author Mitanjo
 */
public class CreateAccount {
    public static void main(String[] args) {
        Session session = HibernateUtil.getSessionFactory().getCurrentSession();
        Transaction tx = null;

        try {
            tx = session.beginTransaction();

            Profile p = new Profile();
            p.setSzLogin("admin");
            p.setSzPwd("admin");
            session.save(p);

            tx.commit();
        }catch(HibernateException e) {
            e.printStackTrace();
        }
    }
}
