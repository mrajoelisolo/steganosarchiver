package core;

import application.frames.*;
import hibernate.cfg.HibernateUtil;
import java.awt.EventQueue;
import lwcanvas.util.LWMakeNimbusLookNFeel;

/**
 *
 * @author Mitanjo
 */
public class Main {
    public static void main(String[] args) {
        EventQueue.invokeLater(new Runnable() {
            public void run() {
                LWMakeNimbusLookNFeel.getInstance().makeIt();
                HibernateUtil.getSessionFactory().openSession();
                new SplashScreen().setVisible(true);
                //new MenuFrame().setVisible(true);
                //new EncryptionFrame().setVisible(true);
                //new PersonManagementFrame(null, new Profile()).setVisible(true);                
            }
        });
    }
}
