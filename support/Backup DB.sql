-- MySQL Administrator dump 1.4
--
-- ------------------------------------------------------
-- Server version	5.0.67-community-nt


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;


--
-- Create schema hermesproject
--

CREATE DATABASE IF NOT EXISTS hermesproject;
USE hermesproject;

--
-- Definition of table `person`
--

DROP TABLE IF EXISTS `person`;
CREATE TABLE `person` (
  `idPerson` bigint(20) unsigned NOT NULL auto_increment,
  `firstName` varchar(45) default NULL,
  `lastName` varchar(45) default NULL,
  `birthDate` varchar(45) default NULL,
  `address` varchar(45) default NULL,
  `contact` varchar(45) default NULL,
  `description` varchar(1024) default NULL,
  `idProfile` varchar(45) NOT NULL,
  PRIMARY KEY  USING BTREE (`idPerson`,`idProfile`)
) ENGINE=InnoDB AUTO_INCREMENT=24 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `person`
--

/*!40000 ALTER TABLE `person` DISABLE KEYS */;
INSERT INTO `person` (`idPerson`,`firstName`,`lastName`,`birthDate`,`address`,`contact`,`description`,`idProfile`) VALUES 
 (23,'b5y{J ','iN|s7{ ','Ec47e=Gn>4','_Cuu>q','x5{z;rwOt2;\"\"','b5y{J 6>fwWs4<pj:u%UusOw\'_\'o\\%*U{l;2\'C|kJ0$<\'x>w6Bnt=0)7ppI2z9ruF0zE 0Uz{SnyW%|;\'yD{(Ij&K #UvjU_ Bfz-J||Z([YvS?`&0N\"\\4bldm([),6!l\nQf`mja138*N>Erq[!W\'','1');
/*!40000 ALTER TABLE `person` ENABLE KEYS */;


--
-- Definition of table `profile`
--

DROP TABLE IF EXISTS `profile`;
CREATE TABLE `profile` (
  `idProfile` bigint(20) unsigned NOT NULL auto_increment,
  `login` varchar(45) default NULL,
  `pwd` varchar(45) default NULL,
  `privilege` int(10) unsigned default NULL,
  PRIMARY KEY  (`idProfile`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `profile`
--

/*!40000 ALTER TABLE `profile` DISABLE KEYS */;
INSERT INTO `profile` (`idProfile`,`login`,`pwd`,`privilege`) VALUES 
 (1,'ÛÚäâÝÓ','âóçÚÊÏØ',1);
/*!40000 ALTER TABLE `profile` ENABLE KEYS */;




/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
