# Logiciel de cryptage de donn�es d�une base de donn�es;

README :

L'architecture logicielle de ce projet est proche de celle du MVC :
* Les controlleurs sont d�finies par les classes dans les packages cmd; Ils servent de pont entre la vue et le mod�le:
ex:
- Ajout d'un nouvel �tudiant => CmdAddEtudiant
- Supprimer un �tudiant => CmdDeleteEtudiant

* Les mod�les sont repr�sent�es par les classes entit�es dans le package mapping
- Ils repr�sentent les donn�es � afficher sur l'interface
ex: Person (nom, pr�nom ...)

* Les vues sont repr�sent�es par les classes h�ritant JFrame et JDialog
ils se trouvent dans le package application/frames

* Les algorithmes de cryptages sont stock�es dans les classes suivantes :
- SteganosCryptorAlpha (simple, utilise une cha�ne comme cl�)
	-> il faut d'abord appeler la m�thode initialize(cl�) pour initialiser la cl� de cryptage
- SteganosCryptorBeta (compliqu�, utilise une image comme cl�)

* La configuration de la connexion � la base se trouve dans le fichier hibernate.cfg.xml (base, mdp...)

NB :
Le mot de passe par d�faut est : admin/admin

Pr�requis :
- Serveur MySql(Wamp par ex)
- La structure de la base de donn�es se trouve dans le fichier base de donn�es.sql